package com.pingpong.model;

import java.io.Serializable;

public class FriendFollowModel extends BaseModel{
	
	private int friendId;
	private int followerId;
	private String followTime;
	
	public FriendFollowModel(){
		
	}

	public int getFriendId() {
		return friendId;
	}

	public void setFriendId(int friendId) {
		this.friendId = friendId;
	}

	public int getFollowerId() {
		return followerId;
	}

	public void setFollowerId(int followerId) {
		this.followerId = followerId;
	}

	public String getFollowTime() {
		return followTime;
	}

	public void setFollowTime(String followTime) {
		this.followTime = followTime;
	}

	@Override
	public String toString() {
		return "FriendFollow [friendId=" + friendId + ", followerId="
				+ followerId + ", followTime=" + followTime + "]";
	}
	
}
