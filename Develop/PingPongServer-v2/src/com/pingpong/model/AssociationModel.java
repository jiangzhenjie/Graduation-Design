
package com.pingpong.model;

import java.io.Serializable;

public class AssociationModel extends BaseModel {

	private int hallId;
	private int friendId;
	private String association;
	
	public AssociationModel(){
		
	}

	public int getHallId() {
		return hallId;
	}

	public void setHallId(int hallId) {
		this.hallId = hallId;
	}

	public int getFriendId() {
		return friendId;
	}

	public void setFriendId(int friendId) {
		this.friendId = friendId;
	}

	public String getAssociation() {
		return association;
	}

	public void setAssociation(String association) {
		this.association = association;
	}

	@Override
	public String toString() {
		return "Association [hallId=" + hallId + ", friendId=" + friendId
				+ ", association=" + association + "]";
	}
	
}
