package com.pingpong.model;

import java.io.Serializable;

public class InviteModel extends BaseModel{
	private int inviteId;
	private int friendId;
	private int hallId;
	private String inviteTime;
	private String gameTime;
	private String inviteAddress;
	private String inviteText;

	public int getInviteId() {
		return inviteId;
	}

	public void setInviteId(int inviteId) {
		this.inviteId = inviteId;
	}

	public int getFriendId() {
		return friendId;
	}

	public void setFriendId(int friendId) {
		this.friendId = friendId;
	}

	public int getHallId() {
		return hallId;
	}

	public void setHallId(int hallId) {
		this.hallId = hallId;
	}

	public String getInviteTime() {
		return inviteTime;
	}

	public void setInviteTime(String inviteTime) {
		this.inviteTime = inviteTime;
	}

	public String getGameTime() {
		return gameTime;
	}

	public void setGameTime(String gameTime) {
		this.gameTime = gameTime;
	}

	public String getInviteAddress() {
		return inviteAddress;
	}

	public void setInviteAddress(String inviteAddress) {
		this.inviteAddress = inviteAddress;
	}

	public String getInviteText() {
		return inviteText;
	}

	public void setInviteText(String inviteText) {
		this.inviteText = inviteText;
	}

	
	
}
