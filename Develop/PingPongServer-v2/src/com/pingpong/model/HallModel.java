package com.pingpong.model;

import java.util.List;

public class HallModel extends BaseModel{
	private int hallId;
	private String hallName;
	private int hallFans;
	private String hallAddress;
	private String hallCharges;
	private String hallPark;
	private String hallPhone;
	private int hallStatus;
	private String hallLatitude;
	private String hallLongitude;
	private double hallDistance;
	private List<String> hallImages;
	public int getHallId() {
		return hallId;
	}
	public void setHallId(int hallId) {
		this.hallId = hallId;
	}
	public String getHallName() {
		return hallName;
	}
	public void setHallName(String hallName) {
		this.hallName = hallName;
	}
	public int getHallFans() {
		return hallFans;
	}
	public void setHallFans(int hallFans) {
		this.hallFans = hallFans;
	}
	public String getHallAddress() {
		return hallAddress;
	}
	public void setHallAddress(String hallAddress) {
		this.hallAddress = hallAddress;
	}
	public String getHallCharges() {
		return hallCharges;
	}
	public void setHallCharges(String hallCharges) {
		this.hallCharges = hallCharges;
	}
	public String getHallPark() {
		return hallPark;
	}
	public void setHallPark(String hallPark) {
		this.hallPark = hallPark;
	}
	public String getHallPhone() {
		return hallPhone;
	}
	public void setHallPhone(String hallPhone) {
		this.hallPhone = hallPhone;
	}
	public int getHallStatus() {
		return hallStatus;
	}
	public void setHallStatus(int hallStatus) {
		this.hallStatus = hallStatus;
	}
	public String getHallLatitude() {
		return hallLatitude;
	}
	public void setHallLatitude(String hallLatitude) {
		this.hallLatitude = hallLatitude;
	}
	public String getHallLongitude() {
		return hallLongitude;
	}
	public void setHallLongitude(String hallLongitude) {
		this.hallLongitude = hallLongitude;
	}
	public double getHallDistance() {
		return hallDistance;
	}
	public void setHallDistance(double hallDistance) {
		this.hallDistance = hallDistance;
	}
	public List<String> getHallImages() {
		return hallImages;
	}
	public void setHallImages(List<String> hallImages) {
		this.hallImages = hallImages;
	}
	@Override
	public String toString() {
		return "HallModel [hallId=" + hallId + ", hallName=" + hallName
				+ ", hallFans=" + hallFans + ", hallAddress=" + hallAddress
				+ ", hallCharges=" + hallCharges + ", hallPark=" + hallPark
				+ ", hallPhone=" + hallPhone + ", hallStatus=" + hallStatus
				+ ", hallLatitude=" + hallLatitude + ", hallLongitude="
				+ hallLongitude + ", hallDistance=" + hallDistance
				+ ", hallImages=" + hallImages + "]";
	}
}
