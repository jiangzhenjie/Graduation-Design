package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.utils.LongHandler;


public abstract class BaseDao implements DBInterface{
	protected JdbcUtils jdbcUtils;
	protected QueryRunner queryRunner;
	
	public BaseDao(){
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}
	
	@Override
	public long insert(String sql, List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.insert(conn, sql, new LongHandler(),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0L;
	}

	@Override
	public int delete(String sql, List<Object> params) {
		return 0;
	}

	@Override
	public int update(String sql, List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.update(conn, sql, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

}
