package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.model.BaseModel;
import com.pingpong.model.HallModel;
import com.pingpong.model.InformationModel;

public class InformationDao extends BaseDao {

	@Override
	public BaseModel query(String sql, List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			 return queryRunner.query(conn, sql,new BeanHandler<InformationModel>(InformationModel.class),params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}

	@Override
	public List<? extends BaseModel> queryList(String sql, List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			 return queryRunner.query(conn, sql,new BeanListHandler<InformationModel>(InformationModel.class),params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}
	
}
