package com.pingpong.dao;

import java.util.List;

import com.pingpong.model.BaseModel;

public interface DBInterface {
	/**
	 * 插入操作
	 * @param sql 插入操作SQL语句
	 * @param pramas 占位符参数
	 * @return 插入行的生成ID
	 */ 
	public long insert(String sql,List<Object> pramas);
	/**
	 * 删除操作
	 * @param sql 删除操作执行的SQL语句
	 * @param params 占位符参数
	 * @return 影响的行数
	 */
	public int delete(String sql,List<Object> params);
	/**
	 * 更新操作
	 * @param sql 更新操作执行的SQL语句
	 * @param params 占位符参数
	 * @return 影响的行数
	 */
	public int update(String sql,List<Object> params);
	
	/**
	 * 查询操作
	 * @param sql 查询操作执行的SQL语句
	 * @param params 占位符参数
	 * @return 单个结果
	 */
	public BaseModel query(String sql,List<Object> params);
	/**
	 * 查询操作
	 * @param sql 查询操作执行的SQL语句
	 * @param params 占位符参数
	 * @return 结果集
	 */
	public List<? extends BaseModel> queryList(String sql,List<Object> params);
	
}
