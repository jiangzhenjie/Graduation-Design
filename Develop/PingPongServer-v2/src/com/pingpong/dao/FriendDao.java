package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.model.BaseModel;
import com.pingpong.model.FriendModel;

public class FriendDao extends BaseDao {

	@Override
	public BaseModel query(String sql, List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			 return queryRunner.query(conn, sql,new BeanHandler<FriendModel>(FriendModel.class),params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}

	@Override
	public List<FriendModel> queryList(String sql, List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			if(params == null || params.size() == 0){
				return queryRunner.query(conn, sql,new BeanListHandler<FriendModel>(FriendModel.class));
			}else {
				return queryRunner.query(conn, sql,new BeanListHandler<FriendModel>(FriendModel.class),params.toArray());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}
	
}
