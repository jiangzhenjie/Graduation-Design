package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.model.BaseModel;
import com.pingpong.model.HallModel;
import com.pingpong.utils.LongHandler;

/**
 * 球馆Dao,负责处理与球馆相关的数据库操作
 * @author JiangZhenJie
 *
 */
public class HallDao extends BaseDao {

	@Override
	public BaseModel query(String sql, List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			 return queryRunner.query(conn, sql,new BeanHandler<HallModel>(HallModel.class),params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}

	@Override
	public List<? extends BaseModel> queryList(String sql, List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			 return queryRunner.query(conn, sql,new BeanListHandler<HallModel>(HallModel.class),params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}
	
}
