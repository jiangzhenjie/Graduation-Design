package com.pingpong.service;

import java.util.ArrayList;
import java.util.List;

import com.pingpong.dao.HallDao;
import com.pingpong.model.HallModel;

/**
 * 球馆Service,负责处理与球馆相关的全部业务逻辑
 * @author JiangZhenJie
 *
 */
public class HallService extends BaseService{
	
	private HallDao hallDao;
	
	public HallService(){
		hallDao = new HallDao();
	}
	
	/**
	 * 根据球馆ID查询单一球馆
	 * @return
	 */
	public HallModel getHallById(String id){
		String sql = "select * from BallHall where hallId = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		return (HallModel) hallDao.query(sql, params);
	}
	
	/**
	 * 根据地理位置查询球馆
	 * @return
	 */
	public List<HallModel> getHallByLocation(){
		return null;
	}
	/**
	 * 根据分区查询球馆
	 * @return
	 */
	public List<HallModel> getHallByArea(){
		return null;
	}
	/**
	 * 根据名称模糊查询球馆
	 * @return
	 */
	public List<HallModel> getHallByName(){
		return null;
	}
	/**
	 * 根据球友Id查询关联球馆
	 * @return
	 */
	public List<HallModel> getHallByFriendId(){
		return null;
	}
	/**
	 * 增加关联球馆数
	 */
	public void increaseHallFan(){
		
	}
	/**
	 * 注册球馆
	 * @return
	 */
	public HallModel registerHall(){
		return null;
	}
}
