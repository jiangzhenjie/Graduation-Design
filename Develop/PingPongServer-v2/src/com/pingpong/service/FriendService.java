package com.pingpong.service;

import java.util.ArrayList;
import java.util.List;

import com.pingpong.dao.FriendDao;
import com.pingpong.dao.HallDao;
import com.pingpong.model.FriendModel;
import com.pingpong.utils.Result;

public class FriendService extends BaseService {

	private FriendDao friendDao;
	private List<Object> params ;

	public FriendService() {
		friendDao = new FriendDao();
		params = new ArrayList<Object>();
	}

	public FriendModel getFriendByIdOrUserName(String query) {
		String sql = "select * from BallFriend where friendId = ? or friendName = ?";
		params.clear();
		params.add(query);
		params.add(query);
		return (FriendModel) friendDao.query(sql, params);
	}

	public List<FriendModel> getFriendByBatchId(List<String> batchId) {
		String sql = "select * from BallFriend where friendId in ";
		StringBuilder sb = new StringBuilder(sql);
		sb.append("(");
		for (Object obj : batchId) {
			sb.append(obj);
			sb.append(",");
		}
		sb.replace(sb.length() - 1, sb.length(), ")");
		return friendDao.queryList(sql, null);
	}
	
	public FriendModel registerService(String friendName,String friendPhone,String friendPassword){
		String sql = "insert into BallFriend (friendName,friendPhone,friendPassword) values (?,?,?)";
		params.clear();
		params.add(friendName); 
		params.add(friendPhone);
		params.add(friendPassword);
		long friendId = friendDao.insert(sql, params);
		return getFriendByIdOrUserName(friendId+"");
	}

	public void increaseFriendFan(String friendId) {
		String sql = "update BallFriend set friendFans = friendFans + 1 where friendId = ?";
		doIncrease(sql, friendId);
	}

	public void increaseFriendPlay(String friendId) {
		String sql = "update BallFriend set friendPlay = friendPlay + 1 where friendId = ?";
		doIncrease(sql, friendId);
	}

	public void increaseFriendHall(String friendId) {
		String sql = "update BallFriend set friendHall = friendHall + 1 where friendId = ?";
		doIncrease(sql, friendId);
	}

	public void increaseFriendLike(String friendId) {
		String sql = "update BallFriend set friendLike = friendLike + 1 where friendId = ?";
		doIncrease(sql, friendId);
	}
	
	private int doIncrease(String sql,String friendId){
		params.clear();
		params.add(friendId);
		return friendDao.update(sql, params);
	}
}
