package com.pingpong.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.ShowHallService;
import com.pingpong.utils.TextUtils;

public class ShowHallAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/javascript; charset=utf-8");

		String userId = request.getParameter("userId");
		String latitude = request.getParameter("latitude");
		String longitude = request.getParameter("longitude");
		String area = request.getParameter("area");
		String hallName = request.getParameter("hallName");
		String hallId = request.getParameter("hallId");

		ShowHallService service = new ShowHallService();
		String rst = service.showHall(userId, latitude, longitude,
				area, hallName, hallId);
		response.getWriter().print(rst);
	}
}
