package com.pingpong.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.InviteService;

public class InviteAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String friendId = request.getParameter("userId");
		String gameTime = request.getParameter("gameTime");
		String inviteTime = request.getParameter("inviteTime");
		String inviteAddress = request.getParameter("inviteAddress");
		String hallId = request.getParameter("hallId");
		String inviteText = request.getParameter("inviteText");
		String invitePerson = request.getParameter("invitePerson");

		InviteService service = new InviteService();
		String result = service.invite(friendId, gameTime, inviteTime, inviteAddress, hallId, inviteText, invitePerson);
		response.getWriter().print(result);
	}

}
