package com.pingpong.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.UpdateFriendService;

public class UpdateFriendAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userId = request.getParameter("userId");
		String friendSign = request.getParameter("friendSign");
		String friendAddress = request.getParameter("friendAddress");
		String friendAge = request.getParameter("friendAge");
		String friendJob = request.getParameter("friendJob");
		String friendExperiences = request.getParameter("friendExperiences");
		UpdateFriendService service = new UpdateFriendService();
		String result = service.updateFriend(userId, friendSign, friendAddress,
				friendAge, friendJob, friendExperiences);
		response.getWriter().print(result);
	}

}
