package com.pingpong.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.PublishMomentService;

public class PublishMomentAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int hallId = Integer.parseInt(request.getParameter("userId"));
		String momentContent = request.getParameter("momentContent");
		PublishMomentService service = new PublishMomentService();
		String rst = service.publishMoment(hallId, momentContent);
		response.getWriter().print(rst);
	}

}
