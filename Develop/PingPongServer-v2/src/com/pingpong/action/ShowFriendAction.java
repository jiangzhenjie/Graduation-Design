package com.pingpong.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.ShowFriendService;

public class ShowFriendAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/javascript; charset=utf-8");
		
		int friendId = Integer.parseInt(request.getParameter("userId"));
		String userName = request.getParameter("userName");

		System.out.println("userId=" + friendId + " userName=" + userName);

		ShowFriendService service = new ShowFriendService();
		String result = service.showFriend(friendId, userName);
		response.getWriter().print(result);
	}

}
