package com.pingpong.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.RegisterService;
import com.pingpong.utils.TextUtils;

public class RegisterAction extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public RegisterAction() {
		super();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String friendName = request.getParameter("friendName");
		String friendPhone = request.getParameter("friendPhone");
		String friendPassword = request.getParameter("friendPassword");

		String hallName = request.getParameter("hallName");
		String hallPrincipalName = request.getParameter("hallPrincipalName");
		String hallPrincipalPhone = request.getParameter("hallPrincipalPhone");
		String hallLicense = request.getParameter("hallLicense");
		String hallPassword = request.getParameter("hallPassword");

		RegisterService registerService = new RegisterService();
		String result;
		if (!TextUtils.isEmpty(friendName) && !TextUtils.isEmpty(friendPhone)
				&& !TextUtils.isEmpty(friendPassword)) {
			// for friend
			result = registerService.registerFriend(friendName, friendPhone,
					friendPassword);
		} else {
			// for ball hall
			result = registerService.registerHall(hallName, hallPrincipalName,
					hallPrincipalPhone, hallLicense, hallPassword);
		}
		response.getWriter().print(result);
	}

}
