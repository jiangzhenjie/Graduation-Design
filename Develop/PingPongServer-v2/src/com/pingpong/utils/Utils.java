package com.pingpong.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Utils {
	
	public static String sortString(String str,String split){
		String[] strs = str.split(split);
		List<String> list = new ArrayList<String>();
		for(String temp : strs){
			list.add(temp);
		}
		
		Collections.sort(list, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
		
		StringBuilder sb = new StringBuilder();
		for(String temp : list){
			sb.append(temp);
			sb.append(split);
		}
		sb.deleteCharAt(sb.length() - 1);
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	/**
	 * 地域查询字符串模糊化
	 * @return
	 */
	public static String queryStringAreaFuzzy(String str){
		return str.replace("省", "")
				.replace("市", "")
				.replace("区", "")
				.replace("县", "")
				.replace("镇", "")
				.replace("州", "")
				.replace("街道", "")
				.replace("路", "");
	}
}
