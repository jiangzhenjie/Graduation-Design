package com.pingpong.utils;

public class Constant {

	public static final int TYPE_HALL = 1;
	public static final int TYPE_FRIEND = 2;

	public static final int STATUS_ONLINE = 0;
	public static final int STATUS_OFFLINE = 1;
	public static final int STATUS_DEATH = 2;

	public static final int STATUS_INVITING = 0;
	public static final int STATUS_INVITED = 1;
	public static final int STATUS_FINISH = 2;

	public static final int STATUS_NOT_REPLY = 0;
	public static final int STATUS_ALLOW_REPLY = 1;
	public static final int STATUS_REJECT_REPLY = 2;

	public static final int INVITY_ALL = 0;
	public static final int INVITY_FRIEND = 1;

	public static final String URL_SERVER_IMAGE = "http://192.168.1.102:8080/PingPongServer/images";
	public static final String URL_SERVER_PORTRAIt = "http://192.168.1.102:8080/PingPongServer/portrait";

	public static final int IMAGE_THRESHOLD_SIZE = 1 * 1024;
	public static final int IMAGE_MAX_SIZE = 1024 * 1024 * 1024;
	public static final int IMAGE_MAX_COUNT = 6;

	public static final long XIN_GE_ACCESS_ID = 2100099420;
	public static final String XIN_GE_SECRET_KEY = "9997f1cbd18cad23598c1cde4a6827e3";

}
