package com.pingpong.utils;

public class LocationUtils {
	
	private static final double EARTH_RADIUS = 6371.393;
	
	public static final double DISTANCE = 10;
	
	private static double dlng;
	private static double dlat;
	
	/**
	 * 根据当前坐标，算出以当前坐标为中心，边长20Km的正方形的四个点坐标。
	 * @param current 当前坐标
	 * @return 正方形四个点的坐标，次序分别是：左上，右上，左下，右下。
	 */
	public static Location[] findSquareLocation(Location current){
		dlng = 2 * Math.asin(Math.sin(DISTANCE /(2*EARTH_RADIUS)) / Math.cos(deg2rad(current.latitude)));
		dlng = rad2deg(dlng);
		
		dlat = DISTANCE / EARTH_RADIUS;
		dlat = rad2deg(dlat);
		
		Location[] locations = new Location[4];
		Location leftTop = new Location();
		Location rightTop = new Location();
		Location leftBottom = new Location();
		Location rightBottom = new Location();
		
		leftTop.latitude = current.latitude + dlat;
		leftTop.longitude = current.longitude - dlng;
		
		rightTop.latitude = current.latitude + dlat;
		rightTop.longitude = current.longitude + dlng;
		
		leftBottom.latitude = current.latitude - dlat;
		leftBottom.longitude = current.longitude - dlng;
				
		rightBottom.latitude = current.latitude - dlat;
		rightBottom.longitude = current.longitude + dlng;
		
		locations[0] = leftTop;
		locations[1] = rightTop;
		locations[2] = leftBottom;
		locations[3] = rightBottom;
		
		return locations;
		
	}
	
	/**
	 * 计算两个坐标点之间的距离，单位为km.
	 * @param loc1 坐标1
	 * @param loc2 坐标2
	 * @return 两个坐标点的距离，单位为km.
	 */
	public static double distance(Location loc1, Location loc2){
		double R = EARTH_RADIUS; // Radius of the earth in km
		  double dLat = deg2rad(loc2.latitude-loc1.latitude);  // deg2rad below
		  double dLon = deg2rad(loc2.longitude-loc1.longitude); 
		  double a = 
		    Math.sin(dLat/2) * Math.sin(dLat/2) +
		    Math.cos(deg2rad(loc1.latitude)) * Math.cos(deg2rad(loc2.latitude)) * 
		    Math.sin(dLon/2) * Math.sin(dLon/2)
		    ; 
		  double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		  double d = R * c; // Distance in km
		  return d;
	}

	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

	public static void main(String args[]) {
		Location current = new Location(40.058738,116.282409);
		Location[] locations = LocationUtils.findSquareLocation(current);
		for(Location loc : locations){
			System.out.println(loc);
		}
	}
}
