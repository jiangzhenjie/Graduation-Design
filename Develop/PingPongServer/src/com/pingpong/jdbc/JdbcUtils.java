package com.pingpong.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

public class JdbcUtils {

	private static final String USERNAME = "root";
	private static final String PASSWORD = "123456";
	private static final String URL = "jdbc:mysql://120.24.76.148:3306/pingpongdb";
	private static final String DRIVER = "com.mysql.jdbc.Driver";

	private Connection connection;

	public JdbcUtils() {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Connection openConnection() {
		try {
			connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void insert(){
		try {
			PreparedStatement  p = (PreparedStatement) connection.prepareStatement("", Statement.RETURN_GENERATED_KEYS);
			p.getGeneratedKeys();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
