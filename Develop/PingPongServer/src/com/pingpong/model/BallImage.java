package com.pingpong.model;

import java.io.Serializable;

public class BallImage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int STATUS_NORMAL = 0;
	public static final int STATUS_DELETED = 1;
	public static final int STATUS_UNKONW = 2;

	private int imageId;
	private String imageName;
	private int imageSize;
	private int imageUser;
	private int imageType;
	private int imageStatus;

	public BallImage() {

	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public int getImageSize() {
		return imageSize;
	}

	public void setImageSize(int imageSize) {
		this.imageSize = imageSize;
	}

	public int getImageUser() {
		return imageUser;
	}

	public void setImageUser(int imageUser) {
		this.imageUser = imageUser;
	}

	public int getImageType() {
		return imageType;
	}

	public void setImageType(int imageType) {
		this.imageType = imageType;
	}

	public int getImageStatus() {
		return imageStatus;
	}

	public void setImageStatus(int imageStatus) {
		this.imageStatus = imageStatus;
	}

	@Override
	public String toString() {
		return "BallImage [imageId=" + imageId + ", imageName=" + imageName
				+ ", imageSize=" + imageSize + ", imageUser=" + imageUser
				+ ", imageType=" + imageType + ", imageStatus=" + imageStatus
				+ "]";
	}

}
