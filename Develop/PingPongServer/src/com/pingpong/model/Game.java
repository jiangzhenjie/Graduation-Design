package com.pingpong.model;

import java.io.Serializable;

public class Game implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int gameId;
	private int hallId;
	private String gameTime;
	private String gameName;
	private String gameExplain;
	private String gamePlan;
	private String gameAward;
	private String gameApply;

	public Game() {

	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public int getHallId() {
		return hallId;
	}

	public void setHallId(int hallId) {
		this.hallId = hallId;
	}

	public String getGameTime() {
		return this.gameTime;
	}

	public void setGameTime(String gameTime) {
		this.gameTime = gameTime;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getGameExplain() {
		return gameExplain;
	}

	public void setGameExplain(String gameExplain) {
		this.gameExplain = gameExplain;
	}

	public String getGamePlan() {
		return gamePlan;
	}

	public void setGamePlan(String gamePlan) {
		this.gamePlan = gamePlan;
	}

	public String getGameAward() {
		return gameAward;
	}

	public void setGameAward(String gameAward) {
		this.gameAward = gameAward;
	}

	public String getGameApply() {
		return gameApply;
	}

	public void setGameApply(String gameApply) {
		this.gameApply = gameApply;
	}

	@Override
	public String toString() {
		return "Game [gameId=" + gameId + ", hallId=" + hallId + ", gameName="
				+ gameName + ", gameExplain=" + gameExplain + ", gamePlan="
				+ gamePlan + ", gameAward=" + gameAward + ", gameApply="
				+ gameApply + "]";
	}

}
