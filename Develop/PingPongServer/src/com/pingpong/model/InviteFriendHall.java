package com.pingpong.model;

public class InviteFriendHall {

	private BallInvite invite;
	private BallHall hall;
	private BallFriend friend;
	public BallInvite getInvite() {
		return invite;
	}
	public void setInvite(BallInvite invite) {
		this.invite = invite;
	}
	public BallHall getHall() {
		return hall;
	}
	public void setHall(BallHall hall) {
		this.hall = hall;
	}
	public BallFriend getFriend() {
		return friend;
	}
	public void setFriend(BallFriend friend) {
		this.friend = friend;
	}
	
}

