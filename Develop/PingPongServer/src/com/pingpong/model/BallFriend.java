package com.pingpong.model;

import java.io.Serializable;
import java.util.List;

public class BallFriend implements Serializable {

	private int friendId;
	private String friendName;
	private String friendPhone;
	private int friendFans;
	private int friendPlay;
	private String friendProfile;
	private int friendHall;
	private int friendLike;
	private String friendSign;
	private String friendAddress;
	private int friendAge;
	private String friendJob;
	private String friendExperiences;
	private int friendStatus;
	private int isFriend;
	private List<String> albums;

	public BallFriend() {

	}

	public int getFriendId() {
		return friendId;
	}

	public void setFriendId(int friendId) {
		this.friendId = friendId;
	}

	public String getFriendName() {
		return friendName;
	}

	public void setFriendName(String friendName) {
		this.friendName = friendName;
	}

	public String getFriendPhone() {
		return friendPhone;
	}

	public void setFriendPhone(String friendPhone) {
		this.friendPhone = friendPhone;
	}

	public int getFriendFans() {
		return friendFans;
	}

	public void setFriendFans(int friendFans) {
		this.friendFans = friendFans;
	}

	public int getFriendPlay() {
		return friendPlay;
	}

	public void setFriendPlay(int friendPlay) {
		this.friendPlay = friendPlay;
	}

	public String getFriendProfile() {
		return friendProfile;
	}

	public void setFriendProfile(String friendProfile) {
		this.friendProfile = friendProfile;
	}

	public int getFriendHall() {
		return friendHall;
	}

	public void setFriendHall(int friendHall) {
		this.friendHall = friendHall;
	}

	public int getFriendLike() {
		return friendLike;
	}

	public void setFriendLike(int friendLike) {
		this.friendLike = friendLike;
	}

	public String getFriendSign() {
		return friendSign;
	}

	public void setFriendSign(String friendSign) {
		this.friendSign = friendSign;
	}

	public String getFriendAddress() {
		return friendAddress;
	}

	public void setFriendAddress(String friendAddress) {
		this.friendAddress = friendAddress;
	}

	public int getFriendAge() {
		return friendAge;
	}

	public void setFriendAge(int friendAge) {
		this.friendAge = friendAge;
	}

	public String getFriendJob() {
		return friendJob;
	}

	public void setFriendJob(String friendJob) {
		this.friendJob = friendJob;
	}

	public String getFriendExperiences() {
		return friendExperiences;
	}

	public void setFriendExperiences(String friendExperiences) {
		this.friendExperiences = friendExperiences;
	}

	public int getFriendStatus() {
		return friendStatus;
	}

	public void setFriendStatus(int friendStatus) {
		this.friendStatus = friendStatus;
	}

	public void setIsFriend(int isFriend) {
		this.isFriend = isFriend;
	}

	public int getIsFriend() {
		return isFriend;
	}

	public List<String> getAlbums() {
		return albums;
	}

	public void setAlbums(List<String> albums) {
		this.albums = albums;
	}

	@Override
	public String toString() {
		return "BallFriend [friendId=" + friendId + ", friendName="
				+ friendName + ", friendPhone=" + friendPhone
				+ ", friendFans="
				+ friendFans + ", friendPlay=" + friendPlay
				+ ", friendProfile=" + friendProfile + ", friendHall="
				+ friendHall + ", friendLike=" + friendLike + ", friendSign="
				+ friendSign + ", friendAddress=" + friendAddress
				+ ", friendAge=" + friendAge + ", friendJob=" + friendJob
				+ ", friendExperiences=" + friendExperiences
				+ ", friendStatus=" + friendStatus + "]";
	}

}
