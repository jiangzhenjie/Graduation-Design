package com.pingpong.model;

import java.io.Serializable;

public class BallHallMoment implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int momentId;
	private int hallId;
	private String momentTime;
	private String momentContent;

	public BallHallMoment(){
		
	}

	public int getMomentId() {
		return momentId;
	}

	public void setMomentId(int momentId) {
		this.momentId = momentId;
	}

	public int getHallId() {
		return hallId;
	}

	public void setHallId(int hallId) {
		this.hallId = hallId;
	}

	public String getMomentTime() {
		return momentTime;
	}

	public void setMomentTime(String momentTime) {
		this.momentTime = momentTime;
	}

	public String getMomentContent() {
		return momentContent;
	}

	public void setMomentContent(String momentContent) {
		this.momentContent = momentContent;
	}

	@Override
	public String toString() {
		return "BallHallMoment [momentId=" + momentId + ", hallId=" + hallId
				+ ", momentTime=" + momentTime + ", momentContent="
				+ momentContent + "]";
	}
	
}
