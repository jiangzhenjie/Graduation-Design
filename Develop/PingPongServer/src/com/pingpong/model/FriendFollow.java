package com.pingpong.model;

import java.io.Serializable;

public class FriendFollow implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int friendId;
	private int followerId;
	private String followTime;
	
	public FriendFollow(){
		
	}

	public int getFriendId() {
		return friendId;
	}

	public void setFriendId(int friendId) {
		this.friendId = friendId;
	}

	public int getFollowerId() {
		return followerId;
	}

	public void setFollowerId(int followerId) {
		this.followerId = followerId;
	}

	public String getFollowTime() {
		return followTime;
	}

	public void setFollowTime(String followTime) {
		this.followTime = followTime;
	}

	@Override
	public String toString() {
		return "FriendFollow [friendId=" + friendId + ", followerId="
				+ followerId + ", followTime=" + followTime + "]";
	}
	
}
