package com.pingpong.model;

import java.io.Serializable;

public class BallInformation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int infoId;
	private String infoTime;
	private String infoDigest;
	private String infoTitle;
	private String infoContent;

	
	public BallInformation(){
		
	}


	public int getInfoId() {
		return infoId;
	}


	public void setInfoId(int infoId) {
		this.infoId = infoId;
	}


	public String getInfoTime() {
		return infoTime;
	}


	public void setInfoTime(String infoTime) {
		this.infoTime = infoTime;
	}


	public String getInfoDigest() {
		return infoDigest;
	}


	public void setInfoDigest(String infoDigest) {
		this.infoDigest = infoDigest;
	}


	public String getInfoTitle() {
		return infoTitle;
	}


	public void setInfoTitle(String infoTitle) {
		this.infoTitle = infoTitle;
	}


	public String getInfoContent() {
		return infoContent;
	}


	public void setInfoContent(String infoContent) {
		this.infoContent = infoContent;
	}


	@Override
	public String toString() {
		return "BallInformation [infoId=" + infoId + ", infoTime=" + infoTime
				+ ", infoDigest=" + infoDigest + ", infoTitle=" + infoTitle
				+ ", infoContent=" + infoContent + "]";
	}
	
}
