package com.pingpong.model;

import java.io.Serializable;
import java.util.List;

public class BallHall implements Serializable {
	private int hallId;
	private String hallName;
	private int hallFans;
	private String hallAddress;
	private String hallCharges;
	private String hallPark;
	private String hallPhone;
	private int hallStatus;
	private String hallLatitude;
	private String hallLongitude;
	private double hallDistance;
	private List<String> hallImages;

	public BallHall() {

	}

	public List<String> getHallImages() {
		return hallImages;
	}

	public void setHallImages(List<String> hallImages) {
		this.hallImages = hallImages;
	}

	public int getHallId() {
		return hallId;
	}

	public void setHallId(int hallId) {
		this.hallId = hallId;
	}

	public String getHallName() {
		return hallName;
	}

	public void setHallName(String hallName) {
		this.hallName = hallName;
	}

	// public String getHallPrincipalName() {
	// return hallPrincipalName;
	// }
	//
	// public void setHallPrincipalName(String hallPrincipalName) {
	// this.hallPrincipalName = hallPrincipalName;
	// }
	//
	// public String getHallPrincipalPhone() {
	// return hallPrincipalPhone;
	// }
	//
	// public void setHallPrincipalPhone(String hallPrincipalPhone) {
	// this.hallPrincipalPhone = hallPrincipalPhone;
	// }
	//
	// public String getHallLicense() {
	// return hallLicense;
	// }
	//
	// public void setHallLicense(String hallLicense) {
	// this.hallLicense = hallLicense;
	// }
	//
	// public String getHallPassword() {
	// return hallPassword;
	// }
	//
	// public void setHallPassword(String hallPassword) {
	// this.hallPassword = hallPassword;
	// }

	public int getHallFans() {
		return hallFans;
	}

	public void setHallFans(int hallFans) {
		this.hallFans = hallFans;
	}

	public String getHallAddress() {
		return hallAddress;
	}

	public void setHallAddress(String hallAddress) {
		this.hallAddress = hallAddress;
	}

	public String getHallCharges() {
		return hallCharges;
	}

	public void setHallCharges(String hallCharges) {
		this.hallCharges = hallCharges;
	}

	public String getHallPark() {
		return hallPark;
	}

	public void setHallPark(String hallPark) {
		this.hallPark = hallPark;
	}

	public String getHallPhone() {
		return hallPhone;
	}

	public void setHallPhone(String hallPhone) {
		this.hallPhone = hallPhone;
	}

	public int getHallStatus() {
		return hallStatus;
	}

	public void setHallStatus(int hallStatus) {
		this.hallStatus = hallStatus;
	}

	public String getHallLatitude() {
		return hallLatitude;
	}

	public void setHallLatitude(String hallLatitude) {
		this.hallLatitude = hallLatitude;
	}

	public String getHallLongitude() {
		return hallLongitude;
	}

	public void setHallLongitude(String hallLongitude) {
		this.hallLongitude = hallLongitude;
	}
	
	public void setHallDistance(double distance){
		this.hallDistance = distance;
	}
	
	public double getHallDistance(){
		return this.hallDistance;
	}

	@Override
	public String toString() {
		return "BallHall [hallId=" + hallId + ", hallName=" + hallName
				+ ", hallFans=" + hallFans + ", hallAddress=" + hallAddress
				+ ", hallCharges=" + hallCharges + ", hallPark=" + hallPark
				+ ", hallPhone=" + hallPhone + ", hallStatus=" + hallStatus
				+ ", hallLatitude=" + hallLatitude + ", hallLongitude="
				+ hallLongitude + "]";
	}

}
