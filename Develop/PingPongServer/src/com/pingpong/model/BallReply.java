package com.pingpong.model;

import java.io.Serializable;

public class BallReply implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int inviteId;
	private int friendId;
	private String replyTime;
	private int status;

	public int getInviteId() {
		return inviteId;
	}

	public void setInviteId(int inviteId) {
		this.inviteId = inviteId;
	}

	public int getFriendId() {
		return friendId;
	}

	public void setFriendId(int friendId) {
		this.friendId = friendId;
	}

	public String getReplyTime() {
		return replyTime;
	}

	public void setReplyTime(String replyTime) {
		this.replyTime = replyTime;
	}

	@Override
	public String toString() {
		return "BallReply [inviteId=" + inviteId + ", friendId=" + friendId
				+ ", replyTime=" + replyTime + "]";
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
