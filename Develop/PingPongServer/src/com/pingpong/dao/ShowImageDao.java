package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallImage;

public class ShowImageDao {
	private static final String SQL_QUERY_IMAGE = "select * from ballimage where imageUser = ? and imageType = ? order by time DESC";
	
	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public ShowImageDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}
	
	public List<BallImage> queryImage(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_IMAGE,
					new BeanListHandler<BallImage>(BallImage.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}

}
