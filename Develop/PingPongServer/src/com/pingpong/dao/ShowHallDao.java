package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallHall;
import com.pingpong.model.BallImage;

public class ShowHallDao {

	private static final String SQL_QUERY_BY_ID = "select * from ballhall where hallId = ? ";
	private static final String SQL_QUERY_BY_LOCATION = "select * from ballhall where ((hallLatitude between ? and ?) and (hallLongitude between ? and ?)) ";
	private static final String SQL_QUERY_BY_AREA = "select * from ballhall where hallAddress like ? ";
	private static final String SQL_QUERY_BY_NAME = "select * from ballhall where hallName = ? ";
	private static final String SQL_QUERY_ASSOCIATE = "select * from ballhall , association where ballhall.hallId = association.hallId and association.friendId = ?";

	private static final String SQL_UPDATE_FANS = "update ballHall set hallFans = hallFans + 1 where hallId = ?";
	
	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public ShowHallDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public List<BallHall> queryById(List<Object> params) {
		List<BallHall> ballHalls = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			ballHalls = queryRunner.query(conn, SQL_QUERY_BY_ID,
					new BeanListHandler<BallHall>(BallHall.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return ballHalls;
	}

	public List<BallHall> queryByLocation(List<Object> params) {
		List<BallHall> ballHalls = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			ballHalls = queryRunner.query(conn, SQL_QUERY_BY_LOCATION,
					new BeanListHandler<BallHall>(BallHall.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return ballHalls;
	}

	public List<BallHall> queryByArea(List<Object> params) {
		List<BallHall> ballHalls = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			ballHalls = queryRunner.query(conn, SQL_QUERY_BY_AREA,
					new BeanListHandler<BallHall>(BallHall.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return ballHalls;
	}

	public List<BallHall> queryByName(List<Object> params) {
		List<BallHall> ballHalls = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			ballHalls = queryRunner.query(conn, SQL_QUERY_BY_NAME,
					new BeanListHandler<BallHall>(BallHall.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return ballHalls;
	}

	public List<BallHall> queryAssociate(List<Object> params) {
		List<BallHall> ballHalls = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			ballHalls = queryRunner.query(conn, SQL_QUERY_ASSOCIATE,
					new BeanListHandler<BallHall>(BallHall.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return ballHalls;
	}

	public int updateFans(List<Object> params){
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.update(conn, SQL_UPDATE_FANS, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}
	
}
