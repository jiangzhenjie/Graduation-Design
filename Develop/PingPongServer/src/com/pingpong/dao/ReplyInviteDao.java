package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallReply;
import com.pingpong.utils.IntegerHandler;
import com.pingpong.utils.LongHandler;

public class ReplyInviteDao {

	private static final String SQL_UPDATE = "update ballreply set status = ? where inviteId = ? and friendId = ?";
	private static final String SQL_QUERY_BY_ID = "select * from ballreply where inviteId = ?";
	private static final String SQL_QUERY_BY_ID_FRIEND = "select * from ballreply where inviteId = ? and friendId = ?";
	private static final String SQL_INSERT = "insert into ballreply(inviteId,friendId,replyTime,status) values (?,?,?,?)";

	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public ReplyInviteDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public int update(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		int rst = -1;
		try {
			rst = queryRunner.update(conn,SQL_UPDATE,params.toArray());
		} catch (SQLException e) {
			rst = -1;
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return rst;
	}
	
	public List<BallReply> queryById(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_BY_ID,
					new BeanListHandler<BallReply>(BallReply.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}
	
	public BallReply queryByIdFriend(List<Object> params){
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_BY_ID_FRIEND,
					new BeanHandler<BallReply>(BallReply.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}
	
	public int insert(List<Object> params){
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.insert(conn, SQL_INSERT, new IntegerHandler(), params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}
}
