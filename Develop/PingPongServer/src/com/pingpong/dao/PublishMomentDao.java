package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.utils.LongHandler;

public class PublishMomentDao {

	private static final String SQL_INSERT = "insert into ballhallmoment (hallId , momentTime , momentContent) values (?,?,?)";

	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public PublishMomentDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public long insert(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.insert(conn, SQL_INSERT, new LongHandler(),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}
}
