package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.Game;

public class ShowGameDao {

	private static final String SQL_QUERY_BY_HALL = "select * from game where hallId = ? and gameId >= ? and gameId <= ? order by gameTime desc limit ? ";
	private static final String SQL_QUERY_AREA = "select * from game,ballhall where game.hallId = ballhall.hallId and hallAddress like ?";
	private static final String SQL_SHOW_ASSOCIATION="select * from game , association where association.friendId = ? and association.hallId = game.hallId";
	
	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;
	
	public ShowGameDao(){
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public List<Game> queryByHall(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_BY_HALL,
					new BeanListHandler<Game>(Game.class), params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}

	public List<Game> query(List<Object> params) {
		List<Game> games = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			games = queryRunner.query(conn, SQL_QUERY_BY_HALL,
					new BeanListHandler<Game>(Game.class), params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return games;
	}
	
	public List<Game> showAssociationGame(List<Object> params){
		List<Game> games = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			games = queryRunner.query(conn, SQL_SHOW_ASSOCIATION,
					new BeanListHandler<Game>(Game.class), params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return games;
	}
	
	public List<Game> queryArea(List<Object> params){
		List<Game> games = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			games = queryRunner.query(conn, SQL_QUERY_AREA,
					new BeanListHandler<Game>(Game.class), params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return games;
	}
}
