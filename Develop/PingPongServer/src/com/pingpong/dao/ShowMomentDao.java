package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallHallMoment;

public class ShowMomentDao {

	private static final String SQL_QUERY_MOMENT = "select * from ballhallmoment where hallId = ? and momentId >= ? and momentId <= ? order by momentTime desc limit ? ";
	
	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public ShowMomentDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}
	
	// TODO : 增加判断球馆用户是否存在。
	
	public List<BallHallMoment> queryMoment(List<Object> params){
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_MOMENT,
					new BeanListHandler<BallHallMoment>(BallHallMoment.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}
}
