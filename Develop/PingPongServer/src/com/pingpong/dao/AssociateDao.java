package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallFriend;
import com.pingpong.model.BallHall;
import com.pingpong.utils.LongHandler;

public class AssociateDao {

	private static final String SQL_INSERT = "insert into association (hallId,friendId,associationTime) values (?,?,?)";
	private static final String SQL_SELECT_FRIEND = "select * from ballfriend where friendId = ?";
	private static final String SQL_SELECT_HALL = "select * from ballhall where hallId = ?";

	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public AssociateDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public BallFriend getBallFriend(List<Object> params) {
		BallFriend ballFriend = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			ballFriend = queryRunner.query(conn, SQL_SELECT_FRIEND,
					new BeanHandler<BallFriend>(BallFriend.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return ballFriend;
	}

	public BallHall getBallHall(List<Object> params) {
		BallHall ballHall = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			ballHall = queryRunner
					.query(conn, SQL_SELECT_HALL, new BeanHandler<BallHall>(
							BallHall.class), params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return ballHall;
	}

	public void insertAssociation(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			queryRunner.insert(conn, SQL_INSERT, new LongHandler(), params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
