package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.utils.IntegerHandler;

public class UploadImageDao {

	private static final String SQL_INSERT_IMAGE = "insert into ballimage (imageName,imageSize,imageUser,imageType,time) values (?,?,?,?,?)";
	private static final String SQL_QUERY_IMAGE_COUNT = "select count(imageId) from ballimage where imageUser = ? and imageType = ?";

	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public UploadImageDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public int insertImage(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.update(conn, SQL_INSERT_IMAGE, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}

	public int queryImageCount(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_IMAGE_COUNT,
					new IntegerHandler(), params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}

}
