package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;

import com.pingpong.jdbc.JdbcUtils;

public class DeleteImageDao {

	private static final String SQL_DELETE = "delete from ballimage where imageUser = ? and imageName = ?";
	
	private QueryRunner queryRunner;
	private JdbcUtils jdbcUtils;
	
	public DeleteImageDao() {
		queryRunner = new QueryRunner();
		jdbcUtils = new JdbcUtils();
	}
	
	public int delete(List<Object> params){
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.update(conn, SQL_DELETE, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
}
