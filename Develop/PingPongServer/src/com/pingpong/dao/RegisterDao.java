package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallFriend;
import com.pingpong.utils.LongHandler;

public class RegisterDao {

	private static final String SQL_INSERT_FRIEND = "insert into ballfriend (friendName,friendPhone,friendPassword) values (?,?,?)";
	private static final String SQL_INSERT_HAll = "insert into ballhall (hallName,hallPrincipalName,hallPrincipalPhone,hallLicense,hallPassword) values (?,?,?,?,?)";
	private static final String SQL_QUERY_PHONE = "select * from ballfriend where friendPhone = ?";

	private QueryRunner queryRunner;
	private JdbcUtils jdbcUtils;

	public RegisterDao() {
		queryRunner = new QueryRunner();
		jdbcUtils = new JdbcUtils();
	}

	public long insertFriend(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.insert(conn, SQL_INSERT_FRIEND,
					new LongHandler(), params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}

	public long insertHall(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.insert(conn, SQL_INSERT_HAll, new LongHandler(),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}

	public BallFriend queryPhone(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_PHONE,
					new BeanHandler<BallFriend>(BallFriend.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}
}
