package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallInvite;
import com.pingpong.utils.IntegerHandler;

public class InviteDao {

	private static final String SQL_INSERT = "insert into ballinvite (friendId , gameTime , inviteTime , inviteAddress , hallId, inviteText , inviteType, inviteStatus) values (?,?,?,?,?,?,?,?)";
	private static final String SQL_QUERY = "select * from ballinvite where inviteId = ?";
	private static final String SQL_INSERT_REPLY = "insert into ballreply(inviteId,friendId,replyTime,status) values (?,?,?,?); ";

	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public InviteDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public int insertInvite(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		int rst = 0;
		try {
			rst = queryRunner.insert(conn, SQL_INSERT, new IntegerHandler(),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return rst;
	}

	public int insertReply(Object[][] params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.insertBatch(conn, SQL_INSERT_REPLY, new IntegerHandler(), params);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}

	public BallInvite query(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY,
					new BeanHandler<BallInvite>(BallInvite.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}

}
