package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallFriend;
import com.pingpong.model.FriendFollow;
import com.pingpong.utils.LongHandler;

public class AddFriendDao {

	private static final String SQL_INSERT = "insert into friendfollow (friendId , followerId , followTime) values (?,?,?) ";
	private static final String SQL_QUERY_FRIEND = "select * from ballfriend where friendId = ? or friendPhone = ?";
	private static final String SQL_QUERY = "select * from friendfollow where friendId = ? and followerId = ?";
	private static final String SQL_UPDATE_FRIEND = "update ballfriend set friendFans = friendFans + 1 where friendId = ?";

	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public AddFriendDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public void insert(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			long rst = queryRunner.insert(conn, SQL_INSERT, new LongHandler(),
					params.toArray());
			System.out.println(rst);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
	}

	public BallFriend queryFriend(List<Object> params) {
		BallFriend friend = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			friend = queryRunner.query(conn, SQL_QUERY_FRIEND,
					new BeanHandler<BallFriend>(BallFriend.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return friend;
	}

	public FriendFollow query(List<Object> params) {
		FriendFollow friendFollow = null;
		Connection conn = jdbcUtils.openConnection();
		try {
			friendFollow = queryRunner.query(conn, SQL_QUERY,
					new BeanHandler<FriendFollow>(FriendFollow.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return friendFollow;
	}

	public int update(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner
					.update(conn, SQL_UPDATE_FRIEND, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}
}
