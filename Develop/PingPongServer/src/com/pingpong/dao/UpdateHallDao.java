package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;

import com.pingpong.jdbc.JdbcUtils;

public class UpdateHallDao {

	private static final String SQL_UPDATE = "update ballhall set hallAddress = ? , hallCharges = ? , hallPark = ? , hallPhone = ? where hallId = ?";
	private static final String SQL_UPDATE_LOCATION = "update ballhall set hallLatitude = ? , hallLongitude = ? where hallId = ? ";

	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public UpdateHallDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public int update(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.update(conn, SQL_UPDATE, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}
	
	public int updateLocation(List<Object> params){
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.update(conn, SQL_UPDATE_LOCATION, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}

}
