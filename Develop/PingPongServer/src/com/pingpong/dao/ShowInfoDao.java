package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallInformation;

public class ShowInfoDao {

	private static final String SQL_QUERY = "select * from ballinformation order by infoTime desc limit ?";
	private static final String SQL_QUERY_SINCE = "select * from ballinformation where infoId > ? order by infoTime desc limit ?";
	private static final String SQL_QUERY_BEFORE = "select * from ballinformation where infoId < ? order by infoTime desc limit ?";
	private static final String SQL_QUERY_SINCE_BEFORE = "select * from ballinformation where infoId > ? and infoId < ? order by infoTime desc limit ?";

	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public ShowInfoDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public List<BallInformation> query(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY,
					new BeanListHandler<BallInformation>(BallInformation.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}

	public List<BallInformation> querySince(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_SINCE,
					new BeanListHandler<BallInformation>(BallInformation.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}

	public List<BallInformation> queryBefore(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_BEFORE,
					new BeanListHandler<BallInformation>(BallInformation.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}
	
	public List<BallInformation> querySinceBefore(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_SINCE_BEFORE,
					new BeanListHandler<BallInformation>(BallInformation.class),
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return null;
	}
}
