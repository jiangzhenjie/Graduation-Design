package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallFriend;
import com.pingpong.model.BallHall;

public class LoginDao {

	private static final String SQL_GET_FRIEND = "select * from ballfriend where friendId = ? and friendPassword = ? "
			+ "or friendPhone = ? and friendPassword = ? ";
	private static final String SQL_GET_HALL = "select * from ballhall where hallId = ? and hallPassword = ? ";
	private static final String SQL_UPDATE_FRIEND = "update ballfriend set friendStatus = ? where friendId = ? or friendPhone = ?";
	private static final String SQL_UPDATA_HALL = "update ballhall set hallStatus = ? where hallId = ? ";

	private QueryRunner queryRunner;
	private JdbcUtils jdbcUtils;

	public LoginDao() {
		queryRunner = new QueryRunner();
		jdbcUtils = new JdbcUtils();
	}

	public BallFriend queryFriend(List<Object> params) {
		BallFriend friend = null;
		Connection conn = jdbcUtils.openConnection();
		BeanHandler<BallFriend> beanHandler = new BeanHandler<BallFriend>(
				BallFriend.class);
		try {
			friend = queryRunner.query(conn, SQL_GET_FRIEND, beanHandler,
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return friend;
	}

	public BallHall queryBallHall(List<Object> params) {
		BallHall hall = null;
		Connection conn = jdbcUtils.openConnection();
		BeanHandler<BallHall> beanHandler = new BeanHandler<BallHall>(
				BallHall.class);
		try {
			hall = queryRunner.query(conn, SQL_GET_HALL, beanHandler,
					params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return hall;
	}

	public int updateFriend(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner
					.update(conn, SQL_UPDATE_FRIEND, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return -1;
	}

	public int updateHall(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.update(conn, SQL_UPDATA_HALL, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return -1;

	}
}
