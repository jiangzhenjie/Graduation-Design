package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallFriend;

public class LogoutDao {

	private static final String SQL_UPDATE_FRIEND = "update ballfriend set friendStatus = 1 where friendId = ?";
	private static final String SQL_UPDATE_HALL = "update ballhall set hallStatus = 1 where hallId = ?";

	private QueryRunner queryRunner;
	private JdbcUtils jdbcUtils;

	public LogoutDao() {
		queryRunner = new QueryRunner();
		jdbcUtils = new JdbcUtils();
	}

	public int updateFriend(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner
					.update(conn, SQL_UPDATE_FRIEND, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}

	public int updateHall(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.update(conn, SQL_UPDATE_HALL, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			jdbcUtils.closeConnection();
		}
		return 0;
	}

}
