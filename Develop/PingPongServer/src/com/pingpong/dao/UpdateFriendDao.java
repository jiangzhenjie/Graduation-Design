package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;

import com.pingpong.jdbc.JdbcUtils;

public class UpdateFriendDao {

	private static final String SQL_UPDATE = "update ballfriend set friendSign = ? , friendAddress = ? , friendAge = ? , friendJob = ? , friendExperiences = ? where friendId = ? ";
	private static final String SQL_UPDATE_PORTRAIT = "update ballfriend set friendProfile = ? where friendId = ?";

	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public UpdateFriendDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public int update(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.update(conn, SQL_UPDATE, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			jdbcUtils.closeConnection();
		}
		return 0;
	}

	public int updatePortrait(List<Object> params){
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.update(conn, SQL_UPDATE_PORTRAIT, params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			jdbcUtils.closeConnection();
		}
		return 0;
	}
	
}
