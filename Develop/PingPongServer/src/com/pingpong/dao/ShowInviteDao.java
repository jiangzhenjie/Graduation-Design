package com.pingpong.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.pingpong.jdbc.JdbcUtils;
import com.pingpong.model.BallInvite;
import com.pingpong.model.InviteFriendHall;
import com.pingpong.utils.InviteFriendHallHandler;

public class ShowInviteDao {

	private static final String SQL_QUERY_INVITE = "select * from ballinvite,ballfriend,ballhall " +
			"where ballinvite.friendId=ballfriend.friendId and ballinvite.hallId=ballhall.hallId " +
			"and inviteType=0 and ballinvite.hallId in ";
	
	private static final String SQL_QUETY_PUBLISH_INIVITE = "select * from ballinvite,ballfriend,ballhall " +
			"where ballinvite.friendId=ballfriend.friendId and ballinvite.hallId=ballhall.hallId and ballinvite.friendId = ? ORDER BY inviteTime DESC";
	
	private static final String SQL_QUERY_RECEVIVE_INIVITE="select * from ballinvite,ballfriend,ballhall,ballreply " +
			"where ballinvite.friendId=ballfriend.friendId and ballinvite.hallId=ballhall.hallId and ballreply.inviteId=ballinvite.inviteId " +
			"and ballreply.friendId=? order by inviteTime DESC";
	
	private static final String SQL_QUERY_BY_ID = "select * from ballinvite,ballfriend,ballHall " +
			"where ballinvite.friendId=ballfriend.friendId and ballinvite.hallId=ballhall.hallId and inviteId=? ORDER BY inviteTime DESC";
	
	private JdbcUtils jdbcUtils;
	private QueryRunner queryRunner;

	public ShowInviteDao() {
		jdbcUtils = new JdbcUtils();
		queryRunner = new QueryRunner();
	}

	public List<InviteFriendHall> query(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();

		StringBuilder sb = new StringBuilder(SQL_QUERY_INVITE);
		sb.append("(");
		for (Object obj : params) {
			sb.append(obj + ",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(") ORDER BY inviteTime DESC");

		try {
			return queryRunner.query(conn, sb.toString(),new InviteFriendHallHandler());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<InviteFriendHall> queryPublish(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUETY_PUBLISH_INIVITE,new InviteFriendHallHandler(),params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<InviteFriendHall> queryReceive(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_RECEVIVE_INIVITE,new InviteFriendHallHandler(),params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	public List<InviteFriendHall> queryById(List<Object> params) {
		Connection conn = jdbcUtils.openConnection();
		try {
			return queryRunner.query(conn, SQL_QUERY_BY_ID,new InviteFriendHallHandler(),params.toArray());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
