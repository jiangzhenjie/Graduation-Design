package com.pingpong.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.AddFriendService;

public class AddFriendAction extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int friendId = Integer.parseInt(request.getParameter("userId"));
		String userName = request.getParameter("userName");
		AddFriendService service = new AddFriendService();
		String rst = service.addFriend(friendId, userName);
		response.getWriter().print(rst);
	}

}
