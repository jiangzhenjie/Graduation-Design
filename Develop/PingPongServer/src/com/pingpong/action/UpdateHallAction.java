package com.pingpong.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.UpdateHallService;

/**
 * 更新球馆信息<br/>
 * <br/>
 * 可更新的信息包括：<br/>
 * 球馆地址，球馆资费方式，球馆停车场信息，约场电话
 * 
 * @author JiangZhenJie
 * 
 */
public class UpdateHallAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println(request.getContextPath());
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int userId = Integer.parseInt(request.getParameter("userId"));
		String hallAddress = request.getParameter("hallAddress");
		String hallCharges = request.getParameter("hallCharges");
		String hallPark = request.getParameter("hallPark");
		String hallPhone = request.getParameter("hallPhone");
		UpdateHallService service = new UpdateHallService();
		String result = service.updateHall(userId, hallAddress, hallCharges,
				hallPark, hallPhone);
		response.getWriter().print(result);
	}

}
