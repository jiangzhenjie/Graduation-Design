package com.pingpong.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.pingpong.service.ReplyInviteService;

public class ReplyInviteAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String userId = request.getParameter("userId");
		String inviteId = request.getParameter("inviteId");
		String inviteAgree = request.getParameter("inviteAgree");
		
		ReplyInviteService service = new ReplyInviteService();
		String result =service.replyInvite(userId,inviteId,inviteAgree);
		response.getWriter().print(result);
	}

	
}
