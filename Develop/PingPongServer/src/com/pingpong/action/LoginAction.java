package com.pingpong.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.LoginService;

public class LoginAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/javascript; charset=utf-8");
		
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		int type = Integer.parseInt(request.getParameter("type"));

		System.out.println("userName=" + userName + " password=" + password
				+ " type=" + type);

		LoginService loginService = new LoginService();
		String result = loginService.login(userName, password, type);
		response.getWriter().print(result);
	}

}
