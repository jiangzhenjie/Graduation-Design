package com.pingpong.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.ShowInviteService;

public class ShowInviteAction extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/javascript; charset=utf-8");

		String userId = request.getParameter("userId");
		String latitude = request.getParameter("latitude");
		String longitude = request.getParameter("longitude");
		String count = request.getParameter("count");
		String flag = request.getParameter("flag");

		System.out.println("show invite action: " + "userId="+userId+" flag="+flag);
		
		ShowInviteService service = new ShowInviteService();
		String rst = service.showInvite(userId, latitude, longitude, count,
				flag);
		response.getWriter().print(rst);
	}
}
