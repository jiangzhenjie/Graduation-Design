package com.pingpong.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.GetReplyInviteService;

public class GetReplyInviteAction extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userId = request.getParameter("userId");
		String flag = request.getParameter("flag");
		System.out.println("get reply invite action: " + "userId="+userId+" flag="+flag);
		GetReplyInviteService service = new GetReplyInviteService();
		String rst = service.getReplyInvite(userId, flag);
		response.getWriter().print(rst);
	}

}
