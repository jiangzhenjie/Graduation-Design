package com.pingpong.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pingpong.service.ChatReceiveService;

public class ChatReceiveAction extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String fromUserId = request.getParameter("fromUserId");
		String toUserId = request.getParameter("toUserId");
		String fromName = request.getParameter("fromUserName");
		String toName = request.getParameter("toUserName");
		String message = request.getParameter("message");
		
		ChatReceiveService service = new ChatReceiveService();
		String rst = service.pushMessage(fromUserId, toUserId, fromName, toName, message);
		response.getWriter().print(rst);
	}
}
