package com.pingpong.action;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.pingpong.service.UploadImageService;
import com.pingpong.utils.Constant;
import com.pingpong.utils.Result;

public class UploadImageAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String imagePath;
	private String tempPath;

	private String imageUser;
	private String imageType;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		imagePath = config.getInitParameter("image-path");
		tempPath = config.getInitParameter("image-temp-path");

		ServletContext context = config.getServletContext();

		imagePath = context.getRealPath(imagePath);
		File file = new File(imagePath);
		if (!file.exists()) {
			file.mkdirs();
		}
		
		tempPath = context.getRealPath(tempPath);
		file = new File(tempPath);
		if (!file.exists()) {
			file.mkdir();
		}
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();

		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(Constant.IMAGE_THRESHOLD_SIZE);
		factory.setRepository(new File(tempPath));

		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(Constant.IMAGE_MAX_SIZE);
		try {
			List<FileItem> fileItems = upload.parseRequest(request);
			for (FileItem fileItem : fileItems) {
				if (fileItem.isFormField()) {
					processFormField(fileItem, out);
				} else {
					processUploadFile(fileItem, out);
				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
			out.print(Result.generateOtherResult(e.getMessage()));
		} finally {
			out.close();
		}
	}

	private void processFormField(FileItem item, PrintWriter out) {
		String fieldName = item.getFieldName();
		if (fieldName.equals("userId")) {
			imageUser = item.getString();
		} else if (fieldName.equals("type")) {
			imageType = item.getString();
		}
	}

	private void processUploadFile(FileItem item, PrintWriter out) {
		String imageName = System.currentTimeMillis() + ".jpg";
		File uploadFile = new File(imagePath + File.separator + imageName);
		try {
			item.write(uploadFile);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!uploadFile.exists()) {
			out.print(Result.RESULT_SERVER_ERROR);
			return;
		}
		String imageSize = uploadFile.length() + "";
		UploadImageService service = new UploadImageService();
		String rst = service.uploadImage(imageName, imageSize, imageUser,
				imageType);
		out.print(rst);
	}
}
