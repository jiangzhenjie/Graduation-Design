package com.pingpong.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.ResultSetHandler;

import com.pingpong.model.BallFriend;
import com.pingpong.model.BallHall;
import com.pingpong.model.BallInvite;
import com.pingpong.model.InviteFriendHall;

public class InviteFriendHallHandler implements
		ResultSetHandler<List<InviteFriendHall>> {

	@Override
	public List<InviteFriendHall> handle(ResultSet rs) throws SQLException {
		List<InviteFriendHall> result = new ArrayList<InviteFriendHall>();
		while (rs.next()) {
			InviteFriendHall ifh = new InviteFriendHall();
			BallInvite invite = new BallInvite();
			BallFriend friend = new BallFriend();
			BallHall hall = new BallHall();

			invite.setFriendId(rs.getInt("friendId"));
			invite.setGameTime(rs.getString("gameTime"));
			invite.setHallId(rs.getInt("hallId"));
			invite.setInviteAddress(rs.getString("inviteAddress"));
			invite.setInviteId(rs.getInt("inviteId"));
			invite.setInviteText(rs.getString("inviteText"));
			invite.setInviteTime(rs.getString("inviteTime"));
			invite.setInviteType(rs.getInt("inviteType"));

			friend.setFriendAddress(rs.getString("friendAddress"));
			friend.setFriendAge(rs.getInt("friendAge"));
			friend.setFriendExperiences(rs.getString("friendExperiences"));
			friend.setFriendFans(rs.getInt("friendFans"));
			friend.setFriendHall(rs.getInt("friendHall"));
			friend.setFriendId(rs.getInt("friendId"));
			friend.setFriendJob(rs.getString("friendJob"));
			friend.setFriendLike(rs.getInt("friendLike"));
			friend.setFriendName(rs.getString("friendName"));
			friend.setFriendPhone(rs.getString("friendPhone"));
			friend.setFriendPlay(rs.getInt("friendPlay"));
			friend.setFriendProfile(rs.getString("friendProfile"));
			friend.setFriendSign(rs.getString("friendSign"));

			hall.setHallAddress(rs.getString("hallAddress"));
			hall.setHallCharges(rs.getString("hallCharges"));
			hall.setHallFans(rs.getInt("hallFans"));
			hall.setHallId(rs.getInt("hallId"));
			hall.setHallLatitude(rs.getString("hallLatitude"));
			hall.setHallLongitude(rs.getString("hallLongitude"));
			hall.setHallName(rs.getString("hallName"));
			hall.setHallPark(rs.getString("hallPark"));
			hall.setHallPhone(rs.getString("hallPhone"));

			ifh.setFriend(friend);
			ifh.setHall(hall);
			ifh.setInvite(invite);
			result.add(ifh);
		}
		return result;
	}
}
