package com.pingpong.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Result {

	private static Map<Integer, String> resultMap = new HashMap<Integer, String>();

	// 成功
	public static final int RESULT_SUCCESS = 0;
	// 用户名或密码错误
	public static final int RESULT_USERNAME_PASSWORD_INVALID = 100;
	// 服务器错误
	public static final int RESULT_SERVER_ERROR = 101;
	// 数据不可用
	public static final int RESULT_DATA_INVALID = 102;
	// 参数不合法
	public static final int RESULT_ARGUMENT_ILLEGAL = 103;
	// 重复主键
	public static final int RESULT_DUPLICATE_KEY = 104;
	// 缺乏登录ID
	public static final int RESULT_NEED_LOGIN_ID = 105;
	// 超过阀值
	public static final int RESULT_EXCEED_THRESHOLD = 106;
	// 其他错误
	public static final int RESULT_OTHER_ERROR = 107;
	static {
		resultMap.put(RESULT_SUCCESS, "success");
		resultMap.put(RESULT_USERNAME_PASSWORD_INVALID,
				"username or password error");
		resultMap.put(RESULT_SERVER_ERROR, "server error");
		resultMap.put(RESULT_DATA_INVALID, "data invalid");
		resultMap.put(RESULT_ARGUMENT_ILLEGAL, "argument illegal");
		resultMap.put(RESULT_DUPLICATE_KEY, "duplicate primary key");
		resultMap.put(RESULT_NEED_LOGIN_ID, "lost login ID");
		resultMap.put(Result.RESULT_EXCEED_THRESHOLD,
				"something exceed threshold value");
	}

	public static String getValue(int key) {
		return resultMap.get(key);
	}

	public static String generateBaseResult(int resultCode) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("resultCode", resultCode);
			jsonObject.put("resultMessage", getValue(resultCode));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	public static String generateOtherResult(String resultMessage) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("resultCode", RESULT_OTHER_ERROR);
			jsonObject.put("resultMessage", resultMessage);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	public static String makeIdResult(long id, String key) {
		String base;
		JSONObject jsonObject;
		if (id > 0) {
			base = generateBaseResult(Result.RESULT_SUCCESS);
			try {
				jsonObject = new JSONObject(base);
				jsonObject.put(key, id);
				return jsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			base = generateBaseResult(Result.RESULT_SERVER_ERROR);
			try {
				jsonObject = new JSONObject(base);
				jsonObject.put(key, -1);
				return jsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return "";
	}

	public static String makeIdResult(String key, int errorCode) {
		String base;
		JSONObject jsonObject;
		base = generateBaseResult(errorCode);
		try {
			jsonObject = new JSONObject(base);
			jsonObject.put(key, -1);
			return jsonObject.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String makeListResult(String listKey, List<?> list) {
		String base;
		JSONObject jsonObject;
		try {
			if (list == null) {
				list = new ArrayList<Object>();
			}
			base = generateBaseResult(RESULT_SUCCESS);
			jsonObject = new JSONObject(base);
			JSONArray jsonArray = new JSONArray(list);
			jsonObject.put(listKey, jsonArray);
			return jsonObject.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String makeSimpleResult(int resultCode ,Map<String, Object> map) {
		if (map == null) {
			return "";
		}

		try {
			JSONObject baseJson = new JSONObject(generateBaseResult(resultCode));
			Set<Map.Entry<String, Object>> entrys = map.entrySet();
			for (Map.Entry<String, Object> entry : entrys) {
				String key = entry.getKey();
				Object value = entry.getValue();
				baseJson.put(key, value);
			}
			
			return baseJson.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

}
