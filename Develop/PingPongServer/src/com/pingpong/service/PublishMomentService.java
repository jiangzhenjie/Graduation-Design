package com.pingpong.service;

import java.util.ArrayList;
import java.util.List;

import com.pingpong.dao.PublishMomentDao;
import com.pingpong.utils.Result;

public class PublishMomentService {

	private PublishMomentDao publishMomentDao;

	public PublishMomentService() {
		publishMomentDao = new PublishMomentDao();
	}

	public String publishMoment(int hallId, String momentContent) {
		List<Object> params = new ArrayList<Object>();
		params.add(hallId);
		params.add(System.currentTimeMillis());
		params.add(momentContent);
		long id = publishMomentDao.insert(params);
		return Result.makeIdResult(id, "momentId");
	}

}
