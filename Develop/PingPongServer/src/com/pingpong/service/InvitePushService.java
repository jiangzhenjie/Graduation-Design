package com.pingpong.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.pingpong.utils.Constant;
import com.tencent.xinge.XingeApp;

public class InvitePushService {

	/**
	 * key为用户ID,value为用户名称
	 */
	private Map<String, String> pushAccount;

	public InvitePushService() {
		pushAccount = new HashMap<String, String>();
	}

	public void setPushAccount(Map<String, String> pushAccount) {
		this.pushAccount = pushAccount;
	}

	public void startPush() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Set<Entry<String, String>> entrySet = pushAccount.entrySet();
				for (Entry<String, String> entry : entrySet) {
					XingeApp.pushAccountAndroid(Constant.XIN_GE_ACCESS_ID,
							Constant.XIN_GE_SECRET_KEY, "收到应约",
							entry.getValue() + "应约了你", entry.getKey());
				}
			}
		}).start();
	}

}
