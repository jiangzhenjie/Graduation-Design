package com.pingpong.service;

import java.util.ArrayList;
import java.util.List;

import com.pingpong.dao.PublishGameDao;
import com.pingpong.utils.Result;

public class PublishGameService {

	private PublishGameDao publishGameDao;

	public PublishGameService() {
		publishGameDao = new PublishGameDao();
	}

	public String publishGame(int userId, String gameName, String gameExplain,
			String gamePlan, String gameAward, String gameApply) {
		List<Object> params = new ArrayList<Object>();
		params.add(userId);
		params.add(gameName);
		long time = System.currentTimeMillis();
		System.out.println(time + " --------------");
		params.add(time);
		params.add(gameExplain);
		params.add(gamePlan);
		params.add(gameAward);
		params.add(gameApply);
		long id = publishGameDao.insert(params);
		return Result.makeIdResult(id, "gameId");
	}

}
