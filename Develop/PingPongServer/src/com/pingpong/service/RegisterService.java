package com.pingpong.service;

import java.util.ArrayList;
import java.util.List;

import com.pingpong.dao.RegisterDao;
import com.pingpong.model.BallFriend;
import com.pingpong.utils.Result;

public class RegisterService {

	private RegisterDao registerDao;

	public RegisterService() {
		registerDao = new RegisterDao();
	}

	public String registerFriend(String friendName, String friendPhone,
			String friendPassword) {
		if (checkPhone(friendPhone)) {
			List<Object> params = new ArrayList<Object>();
			params.add(friendName);
			params.add(friendPhone);
			params.add(friendPassword);
			long userId = registerDao.insertFriend(params);
			return Result.makeIdResult(userId, "userId");
		} else {
			return Result.generateBaseResult(Result.RESULT_DATA_INVALID);
		}
	}

	private boolean checkPhone(String phone) {
		List<Object> params = new ArrayList<Object>();
		params.add(phone);
		BallFriend friend = registerDao.queryPhone(params);
		return friend == null;
	}

	public String registerHall(String hallName, String hallPrincipalName,
			String hallPrincipalPhone, String hallLicense, String hallPassword) {
		List<Object> params = new ArrayList<Object>();
		params.add(hallName);
		params.add(hallPrincipalName);
		params.add(hallPrincipalPhone);
		params.add(hallLicense);
		params.add(hallPassword);
		long userId = registerDao.insertHall(params);
		return Result.makeIdResult(userId, "userId");
	}

}
