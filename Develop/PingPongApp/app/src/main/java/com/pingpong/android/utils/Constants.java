package com.pingpong.android.utils;

/**
 * Created by JiangZhenJie on 2015/2/11.
 */
public class Constants {

    public static final int TYPE_HALL = 1;
    public static final int TYPE_FRIEND = 2;

    public static final int MAX_IMAGE_COUNT = 6;
    public static final String URL_LOCAL_ADD_IMAGE = "url_local_add_image";

    public static final long XIN_GE_ACCESS_ID = 2100099420;
    public static final String XIN_GE_SECRET_KEY = "9997f1cbd18cad23598c1cde4a6827e3";

    public static final int FLAG_IS_NOT_FRIEND = 0;
    public static final int FLAG_IS_FRIEND = 1;

    public static final int FLAG_SHOW_PUBLISH_INVITE = 0;
    public static final int FLAG_SHOW_RECEIVE_INVITE = 1;

    public static final int FLAG_READED = 1;
    public static final int FLAG_UNREAD = 0;

    public static final int FLAG_INVITE_NEAR = 0;
    public static final int FLAG_INVITE_FRIEND = 1;

    public static final int FLAG_NOT_ACCEPT_INVITE = 0;
    public static final int FLAG_ACCEPT_INVITE = 1;

    public static final String LINK_CHAR = ",";

    public static final int FRIEND_MAX_IMAGE = 100;

    public static class RequestId {
        public static final int ID_REGISTER = 100;
        public static final int ID_LOGIN = 101;
        public static final int ID_GET_FRIEND_LIST = 102;
        public static final int ID_PUBLISH_MOMENT = 103;
        public static final int ID_PUBLISH_GAME = 104;
        public static final int ID_SHOW_HALL = 105;
        public static final int ID_SHOW_MOMENT = 106;
        public static final int ID_SHOW_GAME = 107;
        public static final int ID_UPDATE_HALL = 108;
        public static final int ID_UPDATE_HALL_LOCATION = 109;
        public static final int ID_DELETE_IMAGE = 200;
        public static final int ID_GET_FRIEND_SINGLE = 201;
        public static final int ID_ASSOCIATE = 202;
        public static final int ID_INVITE = 203;
        public static final int ID_SHOW_INVITE = 204;
        public static final int ID_REPLY_INVITE = 205;
        public static final int ID_LOGOUT = 206;
        public static final int ID_CHAT = 207;
        public static final int ID_SHOW_FRIEND = 208;
        public static final int ID_ADD_FRIEND = 209;
        public static final int ID_UPDATE_FRIEND_AGE = 210;
        public static final int ID_UPDATE_FRIEND_SIGN = 211;
        public static final int ID_UPDATE_FRIEND_ADDRESS = 212;
        public static final int ID_UPDATE_FRIEND_JOB = 213;
        public static final int ID_UPDATE_FRIEND_EXPERIENCE = 214;
        public static final int ID_UPDATE_FRIEND = 215;
    }

    public static class RequestUrl {
        public static final String URL_SERVER = "http://192.168.1.102:8080/PingPongServer";
//        public static final String URL_SERVER = "http://120.24.76.148:8080/PingPongServer";
        public static final String URL_REGISTER = URL_SERVER + "/register";
        public static final String URL_LOGIN = URL_SERVER + "/login";
        public static final String URL_SHOW_FRIEND = URL_SERVER + "/showfriend";
        public static final String URL_UPLOAD_IMAGE = URL_SERVER + "/uploadimage";
        public static final String URL_PUBLISH_MOMENT = URL_SERVER + "/publishmoment";
        public static final String URL_PUBLISH_GAME = URL_SERVER + "/publishgame";
        public static final String URL_SHOW_HALL = URL_SERVER + "/showhall";
        public static final String URL_SHOW_MOMENT = URL_SERVER + "/showmoment";
        public static final String URL_SHOW_GAME = URL_SERVER + "/showgame";
        public static final String URL_UPDATE_HALL = URL_SERVER + "/updatehall";
        public static final String URL_UPDATE_HALL_LOCATION = URL_SERVER + "/updatehalllocation";
        public static final String URL_DELETE_IMAGE = URL_SERVER + "/deleteimage";
        public static final String URL_UPLOAD_FRIEND_PORTRAIT = URL_SERVER + "/uploadfriendportrait";
        public static final String URL_ASSOCIATE = URL_SERVER + "/associate";
        public static final String URL_INVITE = URL_SERVER + "/invite";
        public static final String URL_SHOW_INVITE = URL_SERVER + "/showinvite";
        public static final String URL_REPLY_INVITE = URL_SERVER + "/replyinvite";
        public static final String URL_LOGOUT = URL_SERVER + "/logout";
        public static final String URL_CHAT = URL_SERVER + "/chat";
        public static final String URL_ADD_FRIEND = URL_SERVER + "/addfriend";
        public static final String URL_UPDATE_FRIEND = URL_SERVER + "/updatefriend";
        public static final String URL_PORTRAIT = URL_SERVER + "/portrait";
    }

    public static class ResultCode {
        public static final int RESULT_OK = 0;
        public static final int RESULT_DATA_INVALID = 102;
        public static final int RESULT_OVER_THRESHOLD = 106;
    }

    public static class Action {
        public static final String ACTION_UPDATE_CHAT = "com.pingpong.android.action.UPDATE_CHAT";
        public static final String ACTION_RECEIVE_CHAT = "com.pingpong.android.action.RECEIVE_CHAT";
    }

}
