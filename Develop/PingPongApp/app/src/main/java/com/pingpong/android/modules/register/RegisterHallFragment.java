package com.pingpong.android.modules.register;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pingpong.android.R;
import com.pingpong.android.base.HttpRequestParam;
import com.pingpong.android.model.RegisterModel;
import com.pingpong.android.utils.Constants;
import com.pingpong.android.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterHallFragment extends Fragment implements View.OnClickListener {

    private RegisterActivity mActivity;

    private EditText mHallName;
    private EditText mHallPrincipalName;
    private EditText mHallPrincipalPhone;
    private EditText mHallLicense;
    private EditText mHallPassword;
    private Button mSubmitRegister;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_hall, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (RegisterActivity) activity;
    }

    private void initView(View view) {
        mHallName = (EditText) view.findViewById(R.id.et_hall_name);
        mHallPrincipalName = (EditText) view.findViewById(R.id.et_hall_principal_name);
        mHallPrincipalPhone = (EditText) view.findViewById(R.id.et_hall_principal_phone);
        mHallLicense = (EditText) view.findViewById(R.id.et_hall_license);
        mHallPassword = (EditText) view.findViewById(R.id.et_hall_password);
        mSubmitRegister = (Button) view.findViewById(R.id.btn_submit_register);
        mSubmitRegister.setOnClickListener(this);
    }

    private boolean checkValues() {
        if (TextUtils.isEmpty(mHallName.getText().toString())) {
            Toast.makeText(getActivity(), R.string.hall_name_invalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(mHallPrincipalName.getText().toString())) {
            Toast.makeText(getActivity(), R.string.hall_principal_phone_invalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(mHallPrincipalPhone.getText().toString())) {
            Toast.makeText(getActivity(), R.string.hall_principal_phone_invalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(mHallLicense.getText().toString())) {
            Toast.makeText(getActivity(), R.string.hall_license_invalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(mHallPassword.getText().toString())) {
            Toast.makeText(getActivity(), R.string.hall_password_invalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (mHallPassword.getText().toString().length() < 6 || mHallPassword.getText().toString().length() > 16) {
            Toast.makeText(getActivity(), R.string.password_length_invalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public HttpRequestParam getRegisterHallParams() {
        HttpRequestParam hrp = new HttpRequestParam(Constants.RequestUrl.URL_REGISTER, RegisterModel.class);
        hrp.addParam("hallName", mHallName.getText().toString());
        hrp.addParam("hallPrincipalName", mHallPrincipalName.getText().toString());
        hrp.addParam("hallPrincipalPhone", mHallPrincipalPhone.getText().toString());
        hrp.addParam("hallLicense", mHallLicense.getText().toString());
        hrp.addParam("hallPassword", Utils.encryptByMD5(mHallPassword.getText().toString()));
        return hrp;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit_register:
                if (checkValues()) {
                    mActivity.sendHttpRequest(Constants.RequestId.ID_REGISTER, R.string.common_loading_progress);
                }
                break;
        }
    }
}
