package com.pingpong.android.utils;

import android.util.Log;

/**
 * Created by JiangZhenJie on 2015/2/3.
 */
public class L {

    private static final boolean IS_DEBUG = true;

    private static final String LOG_DEFAULT_TAG = "ping-pong-log";

    public static void i(String msg) {
        if (IS_DEBUG) {
            Log.i(LOG_DEFAULT_TAG, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (IS_DEBUG) {
            Log.i(tag, msg);
        }
    }

    public static void d(String msg) {
        if (IS_DEBUG) {
            Log.d(LOG_DEFAULT_TAG, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (IS_DEBUG) {
            Log.d(tag, msg);
        }
    }

    public static void e(String msg) {
        if (IS_DEBUG) {
            Log.e(LOG_DEFAULT_TAG, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (IS_DEBUG) {
            Log.e(tag, msg);
        }
    }
}
