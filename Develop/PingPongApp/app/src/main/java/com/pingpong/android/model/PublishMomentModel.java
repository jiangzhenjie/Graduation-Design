package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

/**
 * Created by JiangZhenJie on 2015/3/18.
 */
public class PublishMomentModel extends BaseModel {

    private int momentId;
    private String momentContent;

    public void setMomentId(int momentId) {
        this.momentId = momentId;
    }

    public int getMomentId() {
        return this.momentId;
    }

    public void setMomentConent(String content) {
        this.momentContent = content;
    }

    public String getMomentCotnent() {
        return this.momentContent;
    }
}

