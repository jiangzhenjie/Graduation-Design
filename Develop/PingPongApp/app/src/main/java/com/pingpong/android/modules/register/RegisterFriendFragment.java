package com.pingpong.android.modules.register;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pingpong.android.R;
import com.pingpong.android.base.HttpRequestParam;
import com.pingpong.android.model.RegisterModel;
import com.pingpong.android.utils.Constants;
import com.pingpong.android.utils.Utils;

public class RegisterFriendFragment extends Fragment implements View.OnClickListener {

    private RegisterActivity mActivity;

    private EditText mPhoneNumber;
    private EditText mNickName;
    private EditText mPassword;
    private Button mSubmitRegister;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_friend, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (RegisterActivity) activity;
    }

    private void initView(View view) {
        mPhoneNumber = (EditText) view.findViewById(R.id.et_phone_number);
        mNickName = (EditText) view.findViewById(R.id.et_nick_name);
        mPassword = (EditText) view.findViewById(R.id.et_password);
        mSubmitRegister = (Button) view.findViewById(R.id.btn_submit_register);
        mSubmitRegister.setOnClickListener(this);
    }

    private boolean checkValues() {
        String phone = mPhoneNumber.getText().toString();
        String nick = mNickName.getText().toString();
        String password = mPassword.getText().toString();

        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(getActivity(), R.string.phone_invalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(nick)) {
            Toast.makeText(getActivity(), R.string.nick_name_invalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getActivity(), R.string.password_invalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.length() < 6 || password.length() > 16) {
            Toast.makeText(getActivity(), R.string.password_length_invalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public HttpRequestParam getRegisterFriendParams() {
        HttpRequestParam hrp = new HttpRequestParam(Constants.RequestUrl.URL_REGISTER, RegisterModel.class);
        hrp.addParam("friendPhone", mPhoneNumber.getText().toString());
        hrp.addParam("friendName", mNickName.getText().toString());
        hrp.addParam("friendPassword", Utils.encryptByMD5(mPassword.getText().toString()));
        return hrp;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit_register:
                if (checkValues()) {
                    mActivity.sendHttpRequest(Constants.RequestId.ID_REGISTER, R.string.common_loading_progress);
                }
                break;
        }

    }
}
