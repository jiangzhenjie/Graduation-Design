package com.pingpong.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JiangZhenJie on 2015/3/20.
 */
public class Hall implements Parcelable {
    private int hallId;
    private String hallName;
    private int hallFans;
    private String hallCharges;
    private String hallAddress;
    private String hallPark;
    private String hallPhone;
    private String hallLatitude;
    private String hallLongitude;
    private double hallDistance;
    private List<String> hallImages = new ArrayList<>();

    public Hall() {

    }

    public Hall(Parcel parcel) {
        hallId = parcel.readInt();
        hallName = parcel.readString();
        hallFans = parcel.readInt();
        hallCharges = parcel.readString();
        hallAddress = parcel.readString();
        hallPark = parcel.readString();
        hallPhone = parcel.readString();
        hallLatitude = parcel.readString();
        hallLongitude = parcel.readString();
        hallDistance = parcel.readDouble();
        parcel.readStringList(hallImages);
    }

    public int getHallId() {
        return hallId;
    }

    public String getHallName() {
        return hallName;
    }

    public int getHallFans() {
        return hallFans;
    }

    public String getHallCharges() {
        return hallCharges;
    }

    public String getHallAddress() {
        return hallAddress;
    }

    public String getHallPark() {
        return hallPark;
    }

    public String getHallPhone() {
        return hallPhone;
    }

    public String getHallLatitude() {
        return hallLatitude;
    }

    public String getHallLongitude() {
        return hallLongitude;
    }

    public List<String> getHallImages() {
        return hallImages;
    }

    public void setHallId(int hallId) {
        this.hallId = hallId;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public void setHallFans(int hallFans) {
        this.hallFans = hallFans;
    }

    public void setHallCharges(String hallCharges) {
        this.hallCharges = hallCharges;
    }

    public void setHallAddress(String hallAddress) {
        this.hallAddress = hallAddress;
    }

    public void setHallPark(String hallPark) {
        this.hallPark = hallPark;
    }

    public void setHallPhone(String hallPhone) {
        this.hallPhone = hallPhone;
    }

    public void setHallLatitude(String hallLatitude) {
        this.hallLatitude = hallLatitude;
    }

    public void setHallLongitude(String hallLongitude) {
        this.hallLongitude = hallLongitude;
    }

    public void setHallImages(List<String> hallImages) {
        this.hallImages = hallImages;
    }

    public void setHallDistance(double distance) {
        this.hallDistance = distance;
    }

    public double getHallDistance() {
        return hallDistance;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "hallId=" + hallId +
                ", hallName='" + hallName + '\'' +
                ", hallFans=" + hallFans +
                ", hallCharges='" + hallCharges + '\'' +
                ", hallAddress='" + hallAddress + '\'' +
                ", hallPark='" + hallPark + '\'' +
                ", hallPhone='" + hallPhone + '\'' +
                ", hallLatitude='" + hallLatitude + '\'' +
                ", hallLongitude='" + hallLongitude + '\'' +
                ", hallDistance=" + hallDistance +
                ", hallImages=" + hallImages +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(hallId);
        dest.writeString(hallName);
        dest.writeInt(hallFans);
        dest.writeString(hallCharges);
        dest.writeString(hallAddress);
        dest.writeString(hallPark);
        dest.writeString(hallPhone);
        dest.writeString(hallLatitude);
        dest.writeString(hallLongitude);
        dest.writeDouble(hallDistance);
        dest.writeStringList(hallImages);
    }

    public static final Creator<Hall> CREATOR = new Creator<Hall>() {

        @Override
        public Hall createFromParcel(Parcel source) {
            return new Hall(source);
        }

        @Override
        public Hall[] newArray(int size) {
            return new Hall[size];
        }
    };
}
