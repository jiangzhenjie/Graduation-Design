package com.pingpong.android.base;

import android.os.Bundle;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;

/**
 * Created by JiangZhenJie on 2015/3/29.
 */
public abstract class LocateBaseActivity extends NetBaseActivity implements BDLocationListener {

    protected LocationClient mLocationClient = null;
    protected BDLocation mLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBaidu();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLocate();
    }

    protected void initBaidu() {
        mLocationClient = new LocationClient(getApplicationContext());
        mLocationClient.registerLocationListener(this);
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
        option.setCoorType("bd09ll");
        option.setIsNeedAddress(true);
        mLocationClient.setLocOption(option);
        mLocationClient.start();
    }

    protected void startLocate() {
        if (mLocationClient != null && mLocationClient.isStarted()) {
            mLocationClient.requestLocation();
        } else if (mLocationClient != null) {
            mLocationClient.start();
            mLocationClient.requestLocation();
        }
    }

    protected void stopLocate(){
        if (mLocationClient != null) {
            mLocationClient.stop();
        }
    }

}
