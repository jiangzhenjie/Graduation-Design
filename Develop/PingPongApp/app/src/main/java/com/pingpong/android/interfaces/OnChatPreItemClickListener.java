package com.pingpong.android.interfaces;

/**
 * Created by jiangzhenjie on 15/4/18.
 */
public interface OnChatPreItemClickListener {
    public void onChatPreItemClick(int unread);

}
