package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

import java.util.List;

/**
 * Created by JiangZhenJie on 2015/2/25.
 */
public class ShowFriendModel extends BaseModel {

    private List<Friend> friends;

    public void setFriends(List<Friend> friends){
        this.friends = friends;
    }

    public List<Friend> getFriends(){
        return this.friends;
    }

    @Override
    public String toString() {
        return "FriendModel{" +
                "friends=" + friends.toString() +
                '}';
    }
}
