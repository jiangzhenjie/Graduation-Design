package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

import java.util.List;

/**
 * Created by JiangZhenJie on 2015/3/29.
 */
public class ShowInviteModel extends BaseModel {

    private List<InviteInfo> invites;

    public void setInvites(List<InviteInfo> invites) {
        this.invites = invites;
    }

    public List<InviteInfo> getInvites() {
        return invites;
    }
}
