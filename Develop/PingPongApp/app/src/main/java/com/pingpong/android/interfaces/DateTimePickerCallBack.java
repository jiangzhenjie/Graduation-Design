package com.pingpong.android.interfaces;

import android.content.DialogInterface;
import android.widget.DatePicker;
import android.widget.TimePicker;

/**
 * Created by JiangZhenJie on 2015/4/11.
 */
public interface DateTimePickerCallBack {
    public void onClick(DialogInterface dialog, DatePicker datePicker, TimePicker timePicker);
}