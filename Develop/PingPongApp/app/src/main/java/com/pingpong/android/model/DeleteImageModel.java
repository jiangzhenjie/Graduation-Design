package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

/**
 * Created by JiangZhenJie on 2015/3/22.
 */
public class DeleteImageModel extends BaseModel {

    private String imageUrl;

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
