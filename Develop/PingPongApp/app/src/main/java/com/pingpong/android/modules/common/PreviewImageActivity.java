package com.pingpong.android.modules.common;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pingpong.android.R;
import com.pingpong.android.base.SingletonRequestQueue;
import com.pingpong.android.utils.BitmapCache;
import com.pingpong.android.utils.L;

import uk.co.senab.photoview.PhotoViewAttacher;

public class PreviewImageActivity extends Activity {

    private ImageView mImageView;
    private PhotoViewAttacher mAttacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_image);

        mImageView = (ImageView) findViewById(R.id.iv_preview_image);
        mAttacher = new PhotoViewAttacher(mImageView);

        String url = getIntent().getStringExtra("image_url");
        if (TextUtils.isEmpty(url)) {
            return;
        }
        ImageLoader imageLoader = new ImageLoader(SingletonRequestQueue.getInstance(this).getRequestQueue(), BitmapCache.getInstance());
        imageLoader.get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                mImageView.setImageBitmap(response.getBitmap());
                mAttacher.update();
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                L.e("preview image error response : " + error.getMessage());
            }
        });

        mAttacher.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
            @Override
            public void onViewTap(View view, float v, float v2) {
                mImageView.setImageResource(0);
                finish();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mImageView.setImageResource(0);
        System.gc();
    }
}
