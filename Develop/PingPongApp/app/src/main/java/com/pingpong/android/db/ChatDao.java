package com.pingpong.android.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.pingpong.android.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 聊天记录数据表
 */
@Table(name = "chat_record_table")
public class ChatDao extends Model {

    /*多人聊天，将用户id进行升序排序，用&字符连接起来*/

    @Column(name = "fromUserId", index = true)
    public String fromUserId;

    @Column(name = "fromUserName")
    public String fromUserName;

    @Column(name = "toUserId", index = true)
    public String toUserId;

    @Column(name = "toUserName")
    public String toUserName;

    @Column(name = "message")
    public String message;

    @Column(name = "time")
    public String time;

    @Column(name = "portrait")
    public String portrait;

    /**
     * 0表示未读，1表示已读
     */
    @Column(name = "read")
    public int read;


    public static ChatDao json2ChatDao(String jsonString) throws JSONException {
        ChatDao dao = new ChatDao();
        JSONObject json = new JSONObject(jsonString);
        dao.fromUserName = json.getString("fromUserName");
        dao.fromUserId = json.getString("fromUserId");
        dao.toUserName = json.getString("toUserName");
        dao.toUserId = json.getString("toUserId");
        dao.message = json.getString("message");
        dao.time = System.currentTimeMillis() + "";
        dao.read = Constants.FLAG_UNREAD;
        return dao;
    }

    @Override
    public String toString() {
        return "ChatDao{" +
                "fromUserId='" + fromUserId + '\'' +
                ", fromUserName='" + fromUserName + '\'' +
                ", toUserId='" + toUserId + '\'' +
                ", toUserName='" + toUserName + '\'' +
                ", message='" + message + '\'' +
                ", time='" + time + '\'' +
                ", portrait='" + portrait + '\'' +
                ", read=" + read +
                '}';
    }
}