package com.pingpong.android.modules.friend;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.pingpong.android.R;
import com.pingpong.android.model.InviteInfo;

import java.util.List;

public class FriendInviteFragment extends Fragment {

    private ListView mInviteListView;
    private ReplyInviteListener mListener;

    public FriendInviteFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_invite, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ReplyInviteListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.getLocalClassName() + " must implement ReplyInviteListener");
        }
    }

    private void initView(View view) {
        mInviteListView = (ListView) view.findViewById(R.id.lv_invite_list);
        mInviteListView.setAdapter(new FriendInviteAdapter(getActivity(), mListener));
    }

    public void updateUI(List<InviteInfo> invites) {
        if (invites == null || invites.size() == 0) return;
        FriendInviteAdapter adapter = (FriendInviteAdapter) mInviteListView.getAdapter();
        adapter.setInvites(invites);
    }

    public void notifyDataSetChanged(){
        FriendInviteAdapter adapter = (FriendInviteAdapter) mInviteListView.getAdapter();
        adapter.notifyDataSetChanged();
    }

    public void hideInviteButton(){
        FriendInviteAdapter adapter = (FriendInviteAdapter) mInviteListView.getAdapter();
        adapter.setIsHideInviteButton(true);
    }

    public static interface ReplyInviteListener {
        public void replyInvite(InviteInfo info);
    }

}
