package com.pingpong.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JiangZhenJie on 2015/2/24.
 */
public class Friend implements Parcelable {

    private long friendId;
    private String friendName;
    private String friendPhone;
    private int friendFans;
    private int friendPlay;
    private String friendProfile;
    private int friendHall;
    private int friendLike;
    private String friendSign;
    private String friendAddress;
    private int friendAge;
    private String friendJob;
    private String friendExperiences;
    private int isFriend;
    private List<String> albums = new ArrayList<>();
    private int friendStatus;

    public Friend() {

    }

    public Friend(Parcel parcel) {
        friendId = parcel.readLong();
        friendName = parcel.readString();
        friendPhone = parcel.readString();
        friendFans = parcel.readInt();
        friendPlay = parcel.readInt();
        friendProfile = parcel.readString();
        friendHall = parcel.readInt();
        friendLike = parcel.readInt();
        friendSign = parcel.readString();
        friendAddress = parcel.readString();
        friendAge = parcel.readInt();
        friendJob = parcel.readString();
        friendExperiences = parcel.readString();
        isFriend = parcel.readInt();
        friendStatus = parcel.readInt();
        parcel.readStringList(albums);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(friendId);
        dest.writeString(friendName);
        dest.writeString(friendPhone);
        dest.writeInt(friendFans);
        dest.writeInt(friendPlay);
        dest.writeString(friendProfile);
        dest.writeInt(friendHall);
        dest.writeInt(friendLike);
        dest.writeString(friendSign);
        dest.writeString(friendAddress);
        dest.writeInt(friendAge);
        dest.writeString(friendJob);
        dest.writeString(friendExperiences);
        dest.writeInt(isFriend);
        dest.writeInt(friendStatus);
        dest.writeStringList(albums);
    }

    public static final Creator<Friend> CREATOR = new Creator<Friend>() {

        @Override
        public Friend createFromParcel(Parcel source) {
            return new Friend(source);
        }

        @Override
        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };

    public void setFriendId(long friendId) {
        this.friendId = friendId;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public void setFriendPhone(String friendPhone) {
        this.friendPhone = friendPhone;
    }

    public void setFriendFans(int friendFans) {
        this.friendFans = friendFans;
    }

    public void setFriendPlay(int friendPlay) {
        this.friendPlay = friendPlay;
    }

    public void setFriendProfile(String friendProfile) {
        this.friendProfile = friendProfile;
    }

    public void setFriendHall(int friendHall) {
        this.friendHall = friendHall;
    }

    public void setFriendLike(int friendLike) {
        this.friendLike = friendLike;
    }

    public void setFriendSign(String friendSign) {
        this.friendSign = friendSign;
    }

    public void setFriendAddress(String friendAddress) {
        this.friendAddress = friendAddress;
    }

    public void setFriendAge(int friendAge) {
        this.friendAge = friendAge;
    }

    public void setFriendJob(String friendJob) {
        this.friendJob = friendJob;
    }

    public void setFriendExperiences(String friendExperiences) {
        this.friendExperiences = friendExperiences;
    }

    public void setAlbums(List<String> albums) {
        this.albums = albums;
    }

    public long getFriendId() {
        return friendId;
    }

    public String getFriendName() {
        return friendName;
    }

    public String getFriendPhone() {
        return friendPhone;
    }

    public int getFriendFans() {
        return friendFans;
    }

    public int getFriendPlay() {
        return friendPlay;
    }

    public String getFriendProfile() {
        return friendProfile;
    }

    public int getFriendHall() {
        return friendHall;
    }

    public int getFriendLike() {
        return friendLike;
    }

    public String getFriendSign() {
        return friendSign;
    }

    public String getFriendAddress() {
        return friendAddress;
    }

    public int getFriendAge() {
        return friendAge;
    }

    public String getFriendJob() {
        return friendJob;
    }

    public String getFriendExperiences() {
        return friendExperiences;
    }

    public List<String> getAlbums() {
        return albums;
    }

    public int getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(int isFriend) {
        this.isFriend = isFriend;
    }

    public int getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(int friendStatus) {
        this.friendStatus = friendStatus;
    }

    @Override
    public String toString() {
        return "Friend{" +
                "friendId=" + friendId +
                ", friendName='" + friendName + '\'' +
                ", friendPhone='" + friendPhone + '\'' +
                ", friendFans=" + friendFans +
                ", friendPlay=" + friendPlay +
                ", friendProfile='" + friendProfile + '\'' +
                ", friendHall=" + friendHall +
                ", friendLike=" + friendLike +
                ", friendSign='" + friendSign + '\'' +
                ", friendAddress='" + friendAddress + '\'' +
                ", friendAge=" + friendAge +
                ", friendJob='" + friendJob + '\'' +
                ", friendExperiences='" + friendExperiences + '\'' +
                ", albums=" + albums +
                '}';
    }
}
