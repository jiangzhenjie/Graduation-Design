package com.pingpong.android.base;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by JiangZhenJie on 2015/2/11.
 */
public class SingletonRequestQueue {

    private Context mContext;
    private static SingletonRequestQueue mInstance;
    private RequestQueue mRequestQueue;

    private SingletonRequestQueue(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static SingletonRequestQueue getInstance(Context context) {
        if (mInstance == null) {
            synchronized (SingletonRequestQueue.class) {
                if (mInstance == null) {
                    mInstance = new SingletonRequestQueue(context);
                }
            }
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request) {
        getRequestQueue().add(request);
    }
}
