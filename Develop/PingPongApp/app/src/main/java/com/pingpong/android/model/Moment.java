package com.pingpong.android.model;

/**
 * Created by JiangZhenJie on 2015/3/20.
 */
public class Moment {

    private int momentId;
    private int hallId;
    private String momentTime;
    private String momentContent;

    public int getMomentId() {
        return momentId;
    }

    public int getHallId() {
        return hallId;
    }

    public String getMomentTime() {
        return momentTime;
    }

    public String getMomentContent() {
        return momentContent;
    }

    public void setMomentId(int momentId) {
        this.momentId = momentId;
    }

    public void setHallId(int hallId) {
        this.hallId = hallId;
    }

    public void setMomentTime(String momentTime) {
        this.momentTime = momentTime;
    }

    public void setMomentContent(String momentContent) {
        this.momentContent = momentContent;
    }

    @Override
    public String toString() {
        return "Moment{" +
                "momentId=" + momentId +
                ", hallId=" + hallId +
                ", momentTime='" + momentTime + '\'' +
                ", momentContent='" + momentContent + '\'' +
                '}';
    }
}
