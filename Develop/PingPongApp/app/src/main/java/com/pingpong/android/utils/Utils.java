package com.pingpong.android.utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;

import com.google.gson.Gson;
import com.loopj.android.http.Base64;
import com.pingpong.android.base.BaseModel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by JiangZhenJie on 2015/3/21.
 */
public class Utils {

    public static int getScreenWidth(Context context) {
        Activity ac;
        if (context instanceof Activity) {
            ac = (Activity) context;
            DisplayMetrics metrics = new DisplayMetrics();
            ac.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            return metrics.widthPixels;
        } else {
            return 0;
        }
    }

    public static int getScreenHeight(Context context) {
        Activity ac;
        if (context instanceof Activity) {
            ac = (Activity) context;
            DisplayMetrics metrics = new DisplayMetrics();
            ac.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            return metrics.heightPixels;
        } else {
            return 0;
        }
    }

    public static String formatTime(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(time);
        return format.format(date);
    }

    public static String formatTime(String time) {
        if (time == null) return "";
        long t = Long.parseLong(time);
        return formatTime(t);
    }

    public static String getFilePathByContentResolver(Context context, Uri uri) {
        if (null == uri) {
            return null;
        }
        Cursor c = context.getContentResolver().query(uri, null, null, null, null);
        String filePath = null;
        if (null == c) {
            return null;
        }
        try {
            if ((c.getCount() != 1) || !c.moveToFirst()) {
            } else {
                filePath = c.getString(
                        c.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA));
            }
        } finally {
            c.close();
        }
        return filePath;
    }

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static String formatDistance(double distance) {
        String result;
        if (distance < 1) {
            int temp = (int) (distance * 1000);
            result = "约" + temp + "米";
        } else {
            result = "约";
            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            result += decimalFormat.format(distance) + "千米";
        }
        return result;
    }

    public static String encryptByMD5(String plaint) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] result = md5.digest(plaint.getBytes());
            return Base64.encodeToString(result, Base64.DEFAULT);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static BaseModel json2Model(String json, Class clazz) {
        Gson gson = new Gson();
        return (BaseModel) gson.fromJson(json, clazz);
    }

    public static String formatAge(int number) {
        if (number <= 0 || number > 12) return "";
        if (number <= 12) {
            return number + " " + "月";
        } else {
            return (float) (number / 12) + " " + "年";
        }
    }

    public static String inputStreamToString(InputStream stream) {
        if (stream == null) return "";
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int len;
        byte[] buffer = new byte[1024];
        try {
            while ((len = stream.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            return new String(out.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

}
