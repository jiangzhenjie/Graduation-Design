package com.pingpong.android.base;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.baidu.mapapi.SDKInitializer;

/**
 * Created by JiangZhenJie on 2015/2/3.
 */
public class PingPongApp extends Application {

    /**
     * 登录用户的ID，以此Id作为是否登录的依据,小于等于0表示未登录
     */
    private static long loginUserId;
    /**
     * 登录用户的类型
     */
    private static int loginType;

    @Override
    public void onCreate() {
        super.onCreate();
        //初始化DataManager
        DataManager.getInstance(this);
        //百度地图SDK初始化
        SDKInitializer.initialize(getApplicationContext());
        //初始化ActiveAndroid
        ActiveAndroid.initialize(this);
    }

    public void setLoginUserId(long id) {
        if (id > 0) {
            loginUserId = id;
        }else{
            loginUserId = -1;
        }
    }

    public long getLoginUserId() {
        return loginUserId;
    }

    public void setLoginType(int type) {
        loginType = type;
    }

    public int getLoginType() {
        return loginType;
    }
}
