package com.pingpong.android.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pingpong.android.R;
import com.pingpong.android.modules.login.LoginActivity;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by JiangZhenJie on 2015/2/3.
 */
public class BaseActivity extends ActionBarActivity {

    protected Context mContext;

    //Action Bar 组件
    protected ImageButton mLeftNav;
    protected ImageButton mRightNav;
    protected TextView mActionBarTitle;

    protected ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mActionBar = getSupportActionBar();
        customActionBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    protected void reLoginIfNeeded() {
        if (DataManager.getInstance().getLoginUserId() <= 0) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    /**
     * 自定义导航栏，如需修改样式，可重载该方法
     */
    protected void customActionBar() {
        mActionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_action_bar));
    }
}
