package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

import java.util.List;

/**
 * Created by JiangZhenJie on 2015/3/29.
 */
public class InviteInfo extends BaseModel {

    private int inviteId;
    private int friendId;
    private Hall hall;
    private Friend friend;
    private String inviteTime;
    private String gameTime;
    private String inviteAddress;
    private String inviteText;
    private int inviteType;
    private List<Friend> invitePeoples;

    public void setInviteId(int inviteId) {
        this.inviteId = inviteId;
    }

    public void setFriendId(int friendId) {
        this.friendId = friendId;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public void setInviteTime(String inviteTime) {
        this.inviteTime = inviteTime;
    }

    public void setGameTime(String gameTime) {
        this.gameTime = gameTime;
    }

    public void setInviteAddress(String inviteAddress) {
        this.inviteAddress = inviteAddress;
    }

    public void setInviteText(String inviteText) {
        this.inviteText = inviteText;
    }

    public int getInviteId() {
        return inviteId;

    }

    public int getFriendId() {
        return friendId;
    }

    public Hall getHall() {
        return hall;
    }

    public String getInviteTime() {
        return inviteTime;
    }

    public String getGameTime() {
        return gameTime;
    }

    public String getInviteAddress() {
        return inviteAddress;
    }

    public String getInviteText() {
        return inviteText;
    }

    public void setFriend(Friend friend) {
        this.friend = friend;
    }

    public Friend getFriend() {
        return this.friend;
    }

    public List<Friend> getInvitePeoples() {
        return invitePeoples;
    }

    public void setInvitePeoples(List<Friend> invitePeoples) {
        this.invitePeoples = invitePeoples;
    }

    public void setInviteType(int type){
        inviteType = type;
    }

    public int getInviteType(){
        return inviteType;
    }

    @Override
    public String toString() {
        return "ShowInviteModel{" +
                "inviteId=" + inviteId +
                ", friendId=" + friendId +
                ", hall=" + hall +
                ", inviteTime='" + inviteTime + '\'' +
                ", gameTime='" + gameTime + '\'' +
                ", inviteAddress='" + inviteAddress + '\'' +
                ", inviteText='" + inviteText + '\'' +
                '}';
    }
}
