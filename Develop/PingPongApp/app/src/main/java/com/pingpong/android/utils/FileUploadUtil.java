package com.pingpong.android.utils;

import android.os.AsyncTask;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by JiangZhenJie on 2015/2/23.
 */
public class FileUploadUtil {

    private static final String LINE_END = "\r\n";
    private static final String TWO_HYPHENS = "--";
    private static final String BOUNDARY = "*****";

    public static final int STATUS_SUCCESS = 0;
    public static final int STATUS_FILE_NOT_EXISTS = 1;

    public static int uploadFile(String uri){
        UploadTask uploadTask = new UploadTask();
        uploadTask.execute(uri);
        return 0;
    }

    public static int  doUpload(String uri) {
        String fileName;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        File sourceFile = new File(uri);
        fileName = sourceFile.getName();

        if (!sourceFile.isFile()) {
            L.e("upload file fail : file not exists");
            return STATUS_FILE_NOT_EXISTS;
        }

        try {
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            URL url = new URL(Constants.RequestUrl.URL_UPLOAD_IMAGE);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + BOUNDARY);
            conn.setRequestProperty("uploaded_file", fileName);
            dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(TWO_HYPHENS + BOUNDARY + LINE_END);
            dos.writeBytes("Content-Disposition: form-data; name=" + fileName + ";filename=" + fileName + "" + LINE_END);
            dos.writeBytes(LINE_END);
            int len ;
            byte[] buffer = new byte[1024];
            while ((len = fileInputStream.read(buffer)) != -1) {
                dos.write(buffer, 0, len);
            }
            dos.writeBytes(LINE_END);
            dos.writeBytes(TWO_HYPHENS + BOUNDARY + TWO_HYPHENS + LINE_END);

            int serverResponseCode = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();

            Log.i("uploadFile", "HTTP Response is : "
                    + serverResponseMessage + ": " + serverResponseCode);

            if (serverResponseCode == 200) {
                return STATUS_SUCCESS;
            }

            //close the streams //
            fileInputStream.close();
            dos.flush();
            dos.close();

        } catch (MalformedURLException ex) {
            Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
        } catch (Exception e) {
            Log.e("Upload file to server Exception", "Exception : "
                    + e.getMessage(), e);
        }
        return -1;

    }

    private static class UploadTask extends AsyncTask<String,Integer,Integer>{

        @Override
        protected Integer doInBackground(String... params) {
            doUpload(params[0]);
            return 0;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
        }
    }

}
