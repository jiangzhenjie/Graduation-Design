package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

import java.util.List;

/**
 * Created by JiangZhenJie on 2015/3/20.
 */
public class ShowHallModel extends BaseModel {

    private List<Hall> halls;

    public List<Hall> getHalls() {
        return halls;
    }

    public void setHalls(List<Hall> halls) {
        this.halls = halls;
    }
}
