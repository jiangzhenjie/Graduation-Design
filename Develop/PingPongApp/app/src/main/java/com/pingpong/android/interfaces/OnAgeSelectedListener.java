package com.pingpong.android.interfaces;

/**
 * Created by JiangZhenJie on 2015/4/11.
 */
public interface OnAgeSelectedListener {
    public static final int TYPE_MONTH = 0;
    public static final int TYPE_YEAR = 1;

    public void onAgeSelected(int type, int number);
}
