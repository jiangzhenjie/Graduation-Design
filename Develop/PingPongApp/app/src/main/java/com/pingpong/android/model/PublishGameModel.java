package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

/**
 * Created by JiangZhenJie on 2015/3/18.
 */
public class PublishGameModel extends BaseModel {

    private int gameId;

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getGameId() {
        return gameId;
    }
}

