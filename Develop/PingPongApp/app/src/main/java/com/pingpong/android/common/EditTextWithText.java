package com.pingpong.android.common;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by JiangZhenJie on 2015/2/24.
 */
public class EditTextWithText extends LinearLayout {

    public EditTextWithText(Context context) {
        super(context);
    }

    public EditTextWithText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditTextWithText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initView(){

    }
}
