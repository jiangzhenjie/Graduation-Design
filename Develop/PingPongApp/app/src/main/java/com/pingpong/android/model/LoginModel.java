package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

/**
 * Created by JiangZhenJie on 2015/2/11.
 */
public class LoginModel extends BaseModel {

    private Friend friend;
    private Hall hall;

    public Friend getFriend() {
        return friend;
    }

    public Hall getHall() {
        return hall;
    }

    public void setFriend(Friend friend) {
        this.friend = friend;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }
}
