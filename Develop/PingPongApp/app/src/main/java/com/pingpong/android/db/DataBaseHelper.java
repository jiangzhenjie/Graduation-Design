package com.pingpong.android.db;


import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.pingpong.android.base.DataManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by JiangZhenJie on 2015/4/4.
 */
public class DataBaseHelper {

    /**
     * 保存聊天记录
     *
     * @param json 原始json格式数据
     * @param readState 是否已读
     */
    public static void saveChatRecord(final JSONObject json, final int readState) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ChatDao chatDao = new ChatDao();
                    chatDao.fromUserId = json.getString("fromUserId");
                    chatDao.fromUserName = json.getString("fromUserName");
                    chatDao.toUserId = json.getString("toUserId");
                    chatDao.toUserName = json.getString("toUserName");
                    chatDao.message = json.getString("message");
                    chatDao.time = System.currentTimeMillis() + "";
                    chatDao.read = readState;
                    chatDao.save();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void saveChatRecord(final ChatDao chatDao) {
        if (chatDao == null) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                chatDao.save();
            }
        }).start();
    }

    public static List<ChatDao> readChat(String otherId) {
        List<ChatDao> chatDaoList = new Select()
                .from(ChatDao.class)
                .where("(fromUserId = ?", DataManager.getInstance().getLoginUserId())
                .and("toUserId = ?)", otherId)
                .or("(fromUserId = ?", otherId)
                .and("toUserId = ?)", DataManager.getInstance().getLoginUserId())
                .execute();

        /**
         * 排序
         */
        Collections.sort(chatDaoList, new Comparator<ChatDao>() {
            @Override
            public int compare(ChatDao lhs, ChatDao rhs) {
                return lhs.time.compareTo(rhs.time);
            }
        });

        return chatDaoList;
    }

    public static void setAllMessageReaded(final String firstId, final String secondId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                new Update(ChatDao.class)
                        .set("read = 1")
                        .where("read = 0 and ((fromUserId = ? and toUserId = ?) or (toUserId = ? and fromUserId = ?))", firstId, secondId, firstId, secondId)
                        .execute();
            }
        }).start();
    }

    public static void initCityDataBase(String dataSql) {
        // sqlite 遇到;则停止执行SQL,因此需要分开执行
        String[] sqlArray = dataSql.split(";");
        try {
            ActiveAndroid.beginTransaction();
            for (String str : sqlArray) {
                ActiveAndroid.execSQL(str);
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static List<CityDao> getCityByParentIdAndLevel(long parentId, int level) {
        return new Select()
                .from(CityDao.class)
                .where("parent_id = ?", parentId)
                .and("level = ?", level)
                .execute();
    }
}

