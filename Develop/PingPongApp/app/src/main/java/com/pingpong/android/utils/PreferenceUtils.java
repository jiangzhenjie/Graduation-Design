package com.pingpong.android.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.pingpong.android.base.DataManager;

/**
 * Created by JiangZhenJie on 2015/4/4.
 */
public class PreferenceUtils {

    public static final String KEYCHAIN = "keychain";
    public static final String KEYCHAIN_USERNAME = "username";
    public static final String KEYCHAIN_PASSWORD = "password";
    public static final String KEYCHAIN_TYPE = "type";

    public static PreferenceUtils mInstance;

    private PreferenceUtils() {

    }

    public static PreferenceUtils getInstance() {
        if (mInstance == null) {
            synchronized (PreferenceUtils.class) {
                if (mInstance == null) {
                    mInstance = new PreferenceUtils();
                }
            }
        }
        return mInstance;
    }

    /**
     * 保存用户名和密码
     *
     * @param userName 用户账号
     * @param password 用户密码
     * @param type     用户类型
     */
    public void saveUserKeyChain(String userName, String password, int type) {
        SharedPreferences sharePref = DataManager.getInstance().getContext().getSharedPreferences(KEYCHAIN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.putString(KEYCHAIN_USERNAME, userName);
        editor.putString(KEYCHAIN_PASSWORD, password);
        editor.putString(KEYCHAIN_TYPE, type + "");
        editor.apply();
    }

    public String readUserKeyChainByKey(String key) {
        SharedPreferences sharePref = DataManager.getInstance().getContext().getSharedPreferences(KEYCHAIN, Context.MODE_PRIVATE);
        return sharePref.getString(key, "");
    }

    public void clearUserKeyChain() {
        SharedPreferences sharePref = DataManager.getInstance().getContext().getSharedPreferences(KEYCHAIN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePref.edit();
        editor.remove(KEYCHAIN_USERNAME);
        editor.remove(KEYCHAIN_PASSWORD);
        editor.remove(KEYCHAIN_TYPE);
        editor.apply();
    }

}
