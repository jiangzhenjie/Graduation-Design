package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

/**
 * Created by JiangZhenJie on 2015/2/15.
 */
public class RegisterModel extends BaseModel {

    private int userId;

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return this.userId;
    }

}
