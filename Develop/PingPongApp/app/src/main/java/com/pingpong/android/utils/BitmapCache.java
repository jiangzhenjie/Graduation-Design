package com.pingpong.android.utils;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.toolbox.ImageLoader;

/**
 * Created by JiangZhenJie on 2015/3/21.
 */
public class BitmapCache implements ImageLoader.ImageCache {

    private static LruCache<String, Bitmap> mCache;

    private static BitmapCache mInstance;

    private BitmapCache() {
        int maxSize = 10 * 1024 * 1024;
        mCache = new LruCache<String, Bitmap>(maxSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getRowBytes() * value.getHeight();
            }
        };
    }

    public static BitmapCache getInstance(){
        if (mInstance == null){
            synchronized (BitmapCache.class){
                if (mInstance == null){
                    mInstance = new BitmapCache();
                }
            }
        }
        return mInstance;
    }

    public void deleteBitmap(String url){
        mCache.remove(url);
    }

    @Override
    public Bitmap getBitmap(String url) {
        return mCache.get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        mCache.get(url);
    }
}
