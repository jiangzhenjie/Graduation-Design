package com.pingpong.android.db;

import com.activeandroid.Model;

/**
 * Created by jiangzhenjie on 15/4/18.
 */
public class ChatPreDao extends Model {

    public String userName;

    public String userId;

    public String lastMessage;

    public String portrait;

    public int unReadCount;
}
