package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

/**
 * Created by JiangZhenJie on 2015/3/21.
 */
public class UpdateHallModel extends BaseModel {

    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
