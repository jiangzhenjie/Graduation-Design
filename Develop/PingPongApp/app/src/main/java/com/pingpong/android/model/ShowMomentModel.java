package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

import java.util.List;

/**
 * Created by JiangZhenJie on 2015/3/20.
 */
public class ShowMomentModel extends BaseModel {

    private List<Moment> moments;

    public List<Moment> getMoments() {
        return this.moments;
    }

    public void setMoments(List<Moment> moments) {
        this.moments = moments;
    }
}
