package com.pingpong.android.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by jiangzhenjie on 15/4/19.
 */
@Table(name = "locations")
public class CityDao extends Model {

    @Column(name = "name")
    public String name;

    @Column(name = "parent_id")
    public int parentId;

    @Column(name = "level")
    public int level;

}
