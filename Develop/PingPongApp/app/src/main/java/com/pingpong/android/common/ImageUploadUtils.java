package com.pingpong.android.common;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;
import com.pingpong.android.base.DataManager;
import com.pingpong.android.utils.Constants;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by JiangZhenJie on 2015/3/22.
 */
public class ImageUploadUtils {

    public static final int SUCCESS = 0;
    public static final int FAIL = 1;

    /**
     * 上传图片
     *
     * @param path 图片的绝对路径
     * @return 上传结果<br><br/>
     * 0 : 上传成功<br><br/>
     * 1 : 上传失败<br><br/>
     */
    public static void upload(Context context, String path, ResponseHandlerInterface callBacker) {
        try {
            FileInputStream inputStream = new FileInputStream(path);
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams requestParams = new RequestParams();
            requestParams.setContentEncoding("multipart/form-data");
            requestParams.put("image", inputStream);
            requestParams.put("userId", DataManager.getInstance().getLoginUserId());
            requestParams.put("type", DataManager.getInstance().getLoginType());
            client.post(context, Constants.RequestUrl.URL_UPLOAD_IMAGE, requestParams, callBacker);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void uploadPortrait(Context context, String path, ResponseHandlerInterface callBacker){
        try {
            FileInputStream inputStream = new FileInputStream(path);
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams requestParams = new RequestParams();
            requestParams.setContentEncoding("multipart/form-data");
            requestParams.put("image", inputStream);
            requestParams.put("userId", DataManager.getInstance().getLoginUserId());
            client.post(context, Constants.RequestUrl.URL_UPLOAD_FRIEND_PORTRAIT, requestParams, callBacker);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
