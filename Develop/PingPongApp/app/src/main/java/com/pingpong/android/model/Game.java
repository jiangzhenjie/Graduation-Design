package com.pingpong.android.model;

/**
 * Created by JiangZhenJie on 2015/3/21.
 */
public class Game {

    private int gameId;
    private int hallId;
    private String gameTime;
    private String gameName;
    private String gameExplain;
    private String gamePlan;
    private String gameAward;
    private String gameApply;

    public int getGameId() {
        return gameId;
    }

    public int getHallId() {
        return hallId;
    }

    public String getGameTime() {
        return gameTime;
    }

    public String getGameName() {
        return gameName;
    }

    public String getGameExplain() {
        return gameExplain;
    }

    public String getGamePlan() {
        return gamePlan;
    }

    public String getGameAward() {
        return gameAward;
    }

    public String getGameApply() {
        return gameApply;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public void setHallId(int hallId) {
        this.hallId = hallId;
    }

    public void setGameTime(String gameTime) {
        this.gameTime = gameTime;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public void setGameExplain(String gameExplain) {
        this.gameExplain = gameExplain;
    }

    public void setGamePlan(String gamePlan) {
        this.gamePlan = gamePlan;
    }

    public void setGameAward(String gameAward) {
        this.gameAward = gameAward;
    }

    public void setGameApply(String gameApply) {
        this.gameApply = gameApply;
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameId=" + gameId +
                ", hallId=" + hallId +
                ", gameTime=" + gameTime +
                ", gameName='" + gameName + '\'' +
                ", gameExplain='" + gameExplain + '\'' +
                ", gamePlan='" + gamePlan + '\'' +
                ", gameAward='" + gameAward + '\'' +
                ", gameApply='" + gameApply + '\'' +
                '}';
    }
}
