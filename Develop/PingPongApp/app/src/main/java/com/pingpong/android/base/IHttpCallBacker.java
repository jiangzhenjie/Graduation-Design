package com.pingpong.android.base;

/**
 * Created by JiangZhenJie on 2015/2/3.
 */
public interface IHttpCallBacker {
    public HttpRequestParam makeParam(int requestId);

    public void dataReceived(int requestId,BaseModel response);

    public void processHttpException(int httpState);
}
