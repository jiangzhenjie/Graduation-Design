package com.pingpong.android.model;

import com.pingpong.android.base.BaseModel;

import java.util.List;

/**
 * Created by JiangZhenJie on 2015/3/21.
 */
public class ShowGameModel extends BaseModel {

    private List<Game> games;

    public void setGames(List<Game> games){
        this.games = games;
    }

    public List<Game> getGames(){
        return this.games;
    }

}
