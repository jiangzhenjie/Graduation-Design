/*
Navicat MySQL Data Transfer

Source Server         : MyConnection
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : pingpongdb

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-04-06 16:04:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ballhall`
-- ----------------------------
DROP TABLE IF EXISTS `ballhall`;
CREATE TABLE `ballhall` (
  `hallId` int(11) NOT NULL AUTO_INCREMENT,
  `hallName` varchar(128) NOT NULL,
  `hallPrincipalName` varchar(128) NOT NULL,
  `hallPrincipalPhone` varchar(11) NOT NULL,
  `hallLicense` varchar(128) NOT NULL,
  `hallPassword` varchar(128) NOT NULL,
  `hallFans` int(11) DEFAULT NULL,
  `hallAddress` varchar(128) DEFAULT NULL,
  `hallCharges` varchar(128) DEFAULT NULL,
  `hallPark` varchar(128) DEFAULT NULL,
  `hallPhone` varchar(11) DEFAULT NULL,
  `hallStatus` int(11) DEFAULT NULL,
  `hallLatitude` varchar(32) DEFAULT NULL,
  `hallLongitude` varchar(32) DEFAULT NULL,
  `hallOther1` varchar(256) DEFAULT NULL,
  `hallOther2` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`hallId`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ballhall
-- ----------------------------
INSERT INTO `ballhall` VALUES ('10000', '春晖园球馆', '江振杰', '18825193807', '1234567886633', '4QrcOUm6Wau+VuBX8g+IPg==\n', null, '北京市海淀区西北旺春晖园', '20元／时', '内置停车场', '010-2364261', '1', '40.059208', '116.273246', null, null);
