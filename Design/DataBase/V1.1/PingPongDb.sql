/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2015/2/13 22:58:00                           */ 
/* Version:        V1.1                                         */
/*==============================================================*/

/*                      

  数据表名称代码对应关系：

  球馆表			BallHall
  球友表			BallFriend
  球馆动态表		BallHallMoment
  赛事表			Game
  球讯表			BallInformation
  球友球馆关联表	Association
  球友关注表		FriendFollow
  图片表			BallImage
  约球表			BallInvite
  应约表			BallReply

*/


/* 先判断表是否存在，若存在直接删除(注意外键约束) */

drop table if exists BallReply;

drop table if exists BallInvite;

drop table if exists FriendFollow;

drop table if exists Association;

drop table if exists BallHallMoment;

drop table if exists Game;

drop table if exists BallInformation;

drop table if exists BallImage;

drop table if exists BallHall;

drop table if exists BallFriend;


/*
==============
	创建表
==============
*/


/* 创建球馆表（BallHall）*/

create table BallHall
(
   hallId               int not null auto_increment,
   hallName             varchar(128) not null,
   hallPrincipalName    varchar(128) not null,
   hallPrincipalPhone   varchar(11) not null,
   hallLicense		    varchar(128) not null,
   hallPassword         varchar(128) not null,
   hallFans             int default 0,
   hallAddress			varchar(128),
   hallCharges          varchar(128),
   hallPark             varchar(128),
   hallPhone            varchar(11),
   hallStatus			int,
   hallLatitude			varchar(32),
   hallLongitude		varchar(32),
   hallOther1           varchar(256),
   hallOther2           varchar(256),
   primary key (hallId)
)auto_increment=10000;



/* 创建球友表（BallFriend） */

create table BallFriend
(
   friendId         int not null auto_increment,
   friendName       varchar(128) not null,
   friendPhone      varchar(11) not null,
   friendPassword   varchar(128) not null,
   friendFans       int default 0, 
   friendPlay       int default 0,
   friendProfile	varchar(128),
   friendHall		int default 0,
   friendLike       int default 0,
   friendSign       varchar(256),
   friendAddress   varchar(128),
   friendAge        int,
   friendJob        varchar(128),
   friendExperiences varchar(256),
   friendStatus		int,
   friendOther1     varchar(256),
   friendOther2     varchar(256),
   primary key (friendId)
)auto_increment=10000;



/* 创建球馆动态表（BallHallMoment）*/

create table BallHallMoment
(
   momentId             int not null auto_increment,
   hallId         		int not null,
   momentTime           bigint,
   momentContent        varchar(1024),
   momentOther1         varchar(256),
   momentOther2         varchar(256),
   primary key (momentId)
);



/* 创建赛事表（Game） */

create table Game
(
   gameId               int not null auto_increment,
   hallId         		int not null,
   gameTime				bigint,
   gameName             varchar(128) not null,
   gameExplain          varchar(512),
   gamePlan             varchar(512),
   gameAward            varchar(512),
   gameApply            varchar(512),
   gameOther1           varchar(256),
   gameOther2           varchar(256),
   primary key (gameId)
);



/* 创建球讯表（BallInformation） */

create table BallInformation
(
   infoId        int not null auto_increment,
   infoTime      bigint,
   infoFrom      varchar(128),
   infoDigest    varchar(256),
   infoTitle     varchar(128),
   infoContent   varchar(2048),
   infoOther1    varchar(256),
   infoOther2    varchar(256),
   primary key (infoId)
);



/* 创建球友球馆关联表（Association） */

create table Association
(
   hallId               int not null,
   friendId             int not null,
   associationTime      bigint,
   primary key (hallId, friendId)
);



/* 创建球友关注表（FriendFollow） */

create table FriendFollow
(
   friendId             int not null,
   followerId           int not null,
   followTime           bigint,
   primary key (friendId, followerId)
);



/* 创建图片表（BallImage） */

create table BallImage
(
	imageId			int not null auto_increment,
	imageName		varchar(128) not null,
	imageSize		int,
	imageUser		int,
	imageType		int,
	imageStatus		int,
	imageOther1    varchar(256),
    imageOther2    varchar(256),
	primary key(imageId)
);



/* 创建约球表（BallInvite） */

create table BallInvite
(
	inviteId		int not null auto_increment,
	friendId		int not null,
	gameTime		bigint,
	inviteTime      bigint,
	inviteAddress	varchar(128),
	hallId          int not null,
	inviteText		varchar(256),
	inviteStatus	int,
	inviteType      int,
	inviteOther1    varchar(256),
    inviteOther2    varchar(256),
	primary key(inviteId)
);



/* 创建应约表（BallReply） */

create table BallReply
(
	inviteId	int not null,
	friendId	int not null,
	replyTime	bigint,
	status      int,
	primary key(inviteId,friendId)
);




/*
 ===================
 增加外键约束 
 ===================
 */


/* 球馆动态表 */
alter table BallHallMoment add constraint FK_BallHall_BallHallMoment foreign key (hallId)
      references BallHall (hallId) on delete restrict on update restrict;

/* 赛事表 */
alter table Game add constraint FK_BallHall_Game foreign key (hallId)
      references BallHall (hallId) on delete restrict on update restrict;

/* 球友球馆关联表 */
alter table Association add constraint FK_BallHall_Association foreign key (hallId)
      references BallHall (hallId) on delete restrict on update restrict;

alter table Association add constraint FK_BallFriend_Association foreign key (friendId)
      references BallFriend (friendId) on delete restrict on update restrict;

/* 球友关注表 */
alter table FriendFollow add constraint FK_BallFriend_FriendFollow foreign key (friendId)
      references BallFriend (friendId) on delete restrict on update restrict;

alter table FriendFollow add constraint FK_BallFriend_FriendFollow2 foreign key (followerId)
      references BallFriend (friendId) on delete restrict on update restrict;

/* 约球表 */
alter table BallInvite add constraint FK_BallFriend_BallInvite foreign key (friendId)
      references BallFriend(friendId) on delete restrict on update restrict;

alter table BallInvite add constraint FK_BallHall_BallInvite foreign key (hallId)
      references BallHall(hallId) on delete restrict on update restrict;

/* 应约表 */
alter table BallReply add constraint FK_BallInvite_BallReply foreign key (inviteId)
	  references BallInvite (inviteId) on delete restrict on update restrict;

alter table BallReply add constraint FK_BallFriend_BallReply foreign key (friendId)
	  references BallFriend (friendId) on delete restrict on update restrict;