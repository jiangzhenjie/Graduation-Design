/*
Navicat MySQL Data Transfer

Source Server         : MyConnection
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : pingpongdb

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-04-06 16:04:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `friendfollow`
-- ----------------------------
DROP TABLE IF EXISTS `friendfollow`;
CREATE TABLE `friendfollow` (
  `friendId` int(11) NOT NULL,
  `followerId` int(11) NOT NULL,
  `followTime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`friendId`,`followerId`),
  KEY `FK_BallFriend_FriendFollow2` (`followerId`),
  CONSTRAINT `FK_BallFriend_FriendFollow` FOREIGN KEY (`friendId`) REFERENCES `ballfriend` (`friendId`),
  CONSTRAINT `FK_BallFriend_FriendFollow2` FOREIGN KEY (`followerId`) REFERENCES `ballfriend` (`friendId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of friendfollow
-- ----------------------------
INSERT INTO `friendfollow` VALUES ('10000', '10001', '1428227284181');
INSERT INTO `friendfollow` VALUES ('10001', '10002', '1428238243667');
INSERT INTO `friendfollow` VALUES ('10001', '10003', '1428238235215');
