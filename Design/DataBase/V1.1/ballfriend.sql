/*
Navicat MySQL Data Transfer

Source Server         : MyConnection
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : pingpongdb

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-04-06 16:04:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ballfriend`
-- ----------------------------
DROP TABLE IF EXISTS `ballfriend`;
CREATE TABLE `ballfriend` (
  `friendId` int(11) NOT NULL AUTO_INCREMENT,
  `friendName` varchar(128) NOT NULL,
  `friendPhone` varchar(11) NOT NULL,
  `friendPassword` varchar(128) NOT NULL,
  `friendFans` int(11) DEFAULT NULL,
  `friendPlay` int(11) DEFAULT NULL,
  `friendProfile` varchar(128) DEFAULT NULL,
  `friendHall` int(11) DEFAULT NULL,
  `friendLike` int(11) DEFAULT NULL,
  `friendSign` varchar(256) DEFAULT NULL,
  `friendAddress` varchar(128) DEFAULT NULL,
  `friendAge` int(11) DEFAULT NULL,
  `friendJob` varchar(128) DEFAULT NULL,
  `friendExperiences` varchar(256) DEFAULT NULL,
  `friendStatus` int(11) DEFAULT NULL,
  `friendOther1` varchar(256) DEFAULT NULL,
  `friendOther2` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`friendId`)
) ENGINE=InnoDB AUTO_INCREMENT=10004 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ballfriend
-- ----------------------------
INSERT INTO `ballfriend` VALUES ('10000', '轻描淡写', '18825193807', '4QrcOUm6Wau+VuBX8g+IPg==\n', null, null, null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `ballfriend` VALUES ('10001', '哈哈哈', '13922033135', '4QrcOUm6Wau+VuBX8g+IPg==\n', null, null, null, null, null, null, null, null, null, null, '0', null, null);
INSERT INTO `ballfriend` VALUES ('10002', '嗨嗨嗨', '13727846433', '4QrcOUm6Wau+VuBX8g+IPg==\n', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `ballfriend` VALUES ('10003', '呵呵呵', '18218605466', '4QrcOUm6Wau+VuBX8g+IPg==\n', null, null, null, null, null, null, null, null, null, null, null, null, null);
