/*
Navicat MySQL Data Transfer

Source Server         : MyConnection
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : pingpongdb

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-04-07 22:10:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ballreply`
-- ----------------------------
DROP TABLE IF EXISTS `ballreply`;
CREATE TABLE `ballreply` (
  `inviteId` int(11) NOT NULL,
  `friendId` int(11) NOT NULL,
  `replyTime` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`inviteId`,`friendId`),
  KEY `FK_BallFriend_BallReply` (`friendId`),
  CONSTRAINT `FK_BallFriend_BallReply` FOREIGN KEY (`friendId`) REFERENCES `ballfriend` (`friendId`),
  CONSTRAINT `FK_BallInvite_BallReply` FOREIGN KEY (`inviteId`) REFERENCES `ballinvite` (`inviteId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ballreply
-- ----------------------------
INSERT INTO `ballreply` VALUES ('1', '10000', '1428307994898', '0');
INSERT INTO `ballreply` VALUES ('3', '10000', '1428312205710', '1');
INSERT INTO `ballreply` VALUES ('3', '10002', '1428312205710', '1');
INSERT INTO `ballreply` VALUES ('3', '10003', '1428312205710', '0');
