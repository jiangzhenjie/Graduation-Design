/*
Navicat MySQL Data Transfer

Source Server         : MyConnection
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : pingpongdb

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-04-07 22:10:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ballinvite`
-- ----------------------------
DROP TABLE IF EXISTS `ballinvite`;
CREATE TABLE `ballinvite` (
  `inviteId` int(11) NOT NULL AUTO_INCREMENT,
  `friendId` int(11) NOT NULL,
  `gameTime` bigint(20) DEFAULT NULL,
  `inviteTime` bigint(20) DEFAULT NULL,
  `inviteAddress` varchar(128) DEFAULT NULL,
  `hallId` int(11) NOT NULL,
  `inviteText` varchar(256) DEFAULT NULL,
  `inviteStatus` int(11) DEFAULT NULL,
  `inviteType` int(11) DEFAULT NULL,
  `inviteOther1` varchar(256) DEFAULT NULL,
  `inviteOther2` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`inviteId`),
  KEY `FK_BallFriend_BallInvite` (`friendId`),
  KEY `FK_BallHall_BallInvite` (`hallId`),
  CONSTRAINT `FK_BallFriend_BallInvite` FOREIGN KEY (`friendId`) REFERENCES `ballfriend` (`friendId`),
  CONSTRAINT `FK_BallHall_BallInvite` FOREIGN KEY (`hallId`) REFERENCES `ballhall` (`hallId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ballinvite
-- ----------------------------
INSERT INTO `ballinvite` VALUES ('1', '10001', '1428653522270', '1428308014156', '北京市海淀区西北旺一街', '10000', '轻，我们来打球吧，好嘛？', '0', '1', null, null);
INSERT INTO `ballinvite` VALUES ('2', '10001', '1429172074189', '1428308092676', '北京市海淀区西北旺一街', '10000', '附近有人来打球吗？', '0', '0', null, null);
INSERT INTO `ballinvite` VALUES ('3', '10001', '1429176214646', '1428312225360', '北京市海淀区西北旺一街', '10000', '来来来！', '0', '1', null, null);
