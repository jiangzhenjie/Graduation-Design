/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2015/2/12 18:04:01                           */
/*==============================================================*/


drop table if exists BallFriend;

drop table if exists BallHall;

drop table if exists BallHallFriend;

drop table if exists BallHallMoment;

drop table if exists BallInformation;

drop table if exists FriendFollow;

drop table if exists Game;

/*==============================================================*/
/* Table: BallFriend                                            */
/*==============================================================*/
create table BallFriend
(
   BallFriendId         int not null,
   BallFriendName       varchar(128) not null,
   BallFriendPhone      varchar(11) not null,
   BallFriendPassword   varchar(128) not null,
   BallFriendFans       int,
   BallFriendPlay       int,
   BallFriendAddress    varchar(128),
   BallFriendHallNumber int,
   BallFriendLike       int,
   BallFriendSign       varchar(256),
   BallFriendLocation   varchar(128),
   BallFriendAge        int,
   BallFriendJob        varchar(128),
   BallFriendExperiences varchar(256),
   BallFriendOther1     varchar(512),
   BallFriendOther2     varchar(512),
   primary key (BallFriendId)
);

/*==============================================================*/
/* Table: BallHall                                              */
/*==============================================================*/
create table BallHall
(
   HallId               int not null,
   HallName             varchar(128) not null,
   HallPrincipalName    varchar(128) not null,
   HallPrincipalPhone   varchar(11) not null,
   HallLicenseNumber    varchar(128) not null,
   HallPassword         varchar(128) not null,
   HallFans             int,
   HallCharges          varchar(128),
   HallPark             varchar(128),
   HallPhone            varchar(11),
   HallOther1           varchar(512),
   HallOther2           varchar(512),
   primary key (HallId)
);

/*==============================================================*/
/* Table: BallHallFriend                                        */
/*==============================================================*/
create table BallHallFriend
(
   BallHallId           int not null,
   BallFriendId         int not null,
   AssociateTime        datetime,
   primary key (BallHallId, BallFriendId)
);

/*==============================================================*/
/* Table: BallHallMoment                                        */
/*==============================================================*/
create table BallHallMoment
(
   MomentId             int not null,
   MomentHallId         int,
   MomentTime           datetime,
   MomentContent        varchar(512),
   MomentOther1         varchar(512),
   MomentOther2         varchar(512),
   primary key (MomentId)
);

/*==============================================================*/
/* Table: BallInformation                                       */
/*==============================================================*/
create table BallInformation
(
   InformationId        int not null,
   InformationTime      datetime,
   InformationFrom      varchar(128),
   InformationDigest    varchar(256),
   InformationTitle     varchar(128),
   InformationContent   varchar(2048),
   InformationOther1    varchar(512),
   InformationOther2    varchar(512),
   primary key (InformationId)
);

/*==============================================================*/
/* Table: FriendFollow                                          */
/*==============================================================*/
create table FriendFollow
(
   SporterId            int not null,
   FollowerId           int not null,
   FollowTime           datetime,
   primary key (SporterId, FollowerId)
);

/*==============================================================*/
/* Table: Game                                                  */
/*==============================================================*/
create table Game
(
   GameId               int not null,
   MomentHallId         int,
   GamaName             varchar(256),
   GameExplain          varchar(1024),
   GamePlan             varchar(1024),
   GameAward            varchar(256),
   GameApply            varchar(256),
   GameOther1           varchar(512),
   GameOther2           varchar(512),
   primary key (GameId)
);

alter table BallHallFriend add constraint FK_BallHallFriend foreign key (BallFriendId)
      references BallFriend (BallFriendId) on delete restrict on update restrict;

alter table BallHallFriend add constraint FK_BallHallFriend2 foreign key (BallHallId)
      references BallHall (HallId) on delete restrict on update restrict;

alter table BallHallMoment add constraint FK_update foreign key (MomentHallId)
      references BallHall (HallId) on delete restrict on update restrict;

alter table FriendFollow add constraint FK_FriendFollow foreign key (FollowerId)
      references BallFriend (BallFriendId) on delete restrict on update restrict;

alter table FriendFollow add constraint FK_FriendFollow2 foreign key (SporterId)
      references BallFriend (BallFriendId) on delete restrict on update restrict;

alter table Game add constraint FK_publish foreign key (MomentHallId)
      references BallHall (HallId) on delete restrict on update restrict;

